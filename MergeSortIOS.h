//
//  MergeSort.h
//  ProgramSamplesIOS
//
//  Created by Paul Cernea on 7/12/15.
//  Copyright (c) 2015 Paul Cernea. All rights reserved.
//

#import <Foundation/Foundation.h>

/*! \file MergeSort.h
 \brief Implements the Merge Sort sorting algorithm.
 */

/** @def The types of elements to be sorted.  These are double by default. */
#define NUM double

/** \brief Instantiate once in order to run Merge Sort.
 *
 */
@interface MergeSort : NSObject
{
    NUM * aux_array;  /**< An auxiliary array for memory management during merging.*/
    int aux_array_size;  /**< The size of the auxiliary array.*/
}

/** \brief Merges two sorted arrays.
 * @param array An array of NUM containing the two sorted arrays
 * to be merged.  They are stored contiguously.
 * @param first_index The first index of the entire array.
 * Also, the first index of
 * the first sorted array to be merged.
 * @param half_index The first index of the second sorted array to be merged.
 * Also, half_index - 1 is the last index of the first sorted array to be
 * merged.
 * @param last_index The last index of the second sorted array to be merged.
 */
-(void) merge : (NUM *) array first_index: (int) first_index half_index: (int) half_index last_index: (int) last_index;

/** \brief Recursively sorts an array.
 *
 * The recursive sort proceeds as follows:  If the array has fewer than two
 * elements, there is nothing more to be done.  Otherwise, split the array
 * in half.  Recursively sort both halves.  Then perform a linear-time
 * merge to combine the arrays.
 * The entire procedure takes O(n * log(n)) time.
 * @param array The array to be merge-sorted.
 * @param first_index The first index of the array.
 * @param last_index The last index of the array.
 */
-(void) mergeSort : (NUM *) array first_index : (int) first_index last_index : (int) last_index;

/** \brief Initializes the auxiliary array as <code>NULL</code> and sets
 * its size to 0.
 */
-(id) init;


/** \brief Frees the memory allocated from calling Merge Sort.
 *
 * Frees the memory allocated from calling Merge Sort and resets the auxiliary array
 * size to 0.
 */
-(void) freeMemory;

/** \brief Performs Merge Sort on an array of length arrayLength.
 *
 * Performs Merge Sort on an array of length arrayLength in O(n * log(n))
 * time, where n = arrayLength.  The method will terminate
 * if it cannot allocate the necessary O(n)
 * extra memory to perform the sort.  It uses
 * mergeSort : (NUM *) : (int) : (int)
 * as a subroutine.
 * @param array The array to be sorted.
 * @param arrayLength The length of the array to be sorted.
 * @throw
 *  An out-of-memory exception is thrown if there is no more memory for
 *  the auxiliary array.
 */
-(void) mergeSort : (NUM *) array withLength: (int) arrayLength;

/** \brief Prints an array of length arrayLength.
 * @param array The array to be printed.
 * @param arrayLength The length of the array to be printed.
 * @return The string that is printed.
 */
+(NSString *) print : (NUM *) array withLength: (int) arrayLength;

/** \brief Tests Merge Sort on an array of 70 pseudo-random numbers.
 *
 * The pseudo-random numbers have had the modulo operator applied to them to
 * make them less than 10,000.  This makes them visually easier to read.  Then
 * they are stored as doubles with 0.5 added to them.  The unsorted and sorted
 * arrays are printed.
 *
 * @return The string displaying the test.
 */
+(NSString *) test_mergesort;

@end
