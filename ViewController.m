//
//  ViewController.m
//  ProgramSamplesIOS
//
//  Created by Paul Cernea on 7/12/15.
//  Copyright (c) 2015 Paul Cernea. All rights reserved.
//

#import "ViewController.h"
#import "MergeSort.h"

@implementation ViewController

- (IBAction) performMergeSort : (id) sender
{
    [console setText : [MergeSort test_mergesort]];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
