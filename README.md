# Instructions #

**C++**
------

Computational Geometry
-----------
The **computational_geometry** folder contains some computational geometry capabilities written in C++.  They can be run on UNIX or Mac OSX operating systems with the bash script contained within.

Currently capabilities include

* A templatized **point_3d** class that has **point_2d** as a derived class.
* The ability to generate a desired set of random points with a mouse click and render them with **OpenGL**.
* The **convex hull** of the points is automatically calculated using Graham's scan.  It is automatically rendered using OpenGL.
* Instructions for the *Computational Geometry* program:
	* Run `bash compile_and_run_geometry.sh` to run the computational geometry suite.  By default, 1000 points are generated.
	* Run `bash compile_and_run_geometry.sh <number-of-points>` to run the computational geometry suite with your desired number of points.  For instance `bash compile_and_run_geometry.sh 200` would run the program with 200 points.
	* Click the window to generate a new point set.
	* The convex hull is automatically generated.
	* Press **Q** or **ESC** to exit. 
* Screenshots from the *Computational Geometry* program are included.

main.cpp
-----------

This file contains the main function for a project involving MergeSort, RedBlackTree, and other C++ programming samples.  The main function runs the relevant tests for each sample.

MergeSort.h
-----------

* Header file contains a C++ implementation of Merge Sort
* Merge Sort is a recursive algorithm that uses the divide-and-conquer paradigm.
* In theory it takes O(*n* log(*n*)) time, where *n* is the size of the array to be sorted.
* This implementation has the bonus that it doesn't reallocate memory unnecessarily, and is templatized.
* Add this header file to a C++ project, and run programSamples::test_mergesort() to see Merge Sort performed on an array of random doubles.

QueensProblem.h
---------------

* Header file contains a C++ solution to the problem of configuring *n* mutually non-attacking queens on an *n* x *n* chessboard.
* As a bonus, rook configurations are also addressed.
* Add this header file to a C++ project, and run programSamples::testChessboard() to see rook configurations for the 3 x 3 case and queen configurations for the 8 x 8 case.

RedBlackTree.h
--------------

* Header file contains a C++ implementation of the Red-Black Tree data structure.
* Tests for the correctness of insertion and deletion are included, involving random insertions and deletions.  The height is also displayed.
* Add this header file to a C++ project, and run programSamples::RedBlackTree<double>::test_red_black_tree() to perform the insertion and deletion tests on a red-black tree.

CountRedBlack.h
-----------

* This file implements a dynamic programming routine for counting the number of red-black trees with a given number of nodes.
* We assume the existence of a root and that every node has two children unless it is a leaf.  Thus there are no red-black trees with an even number of nodes.
* Trees may be isomorphic (symmetric) without being considered identical.
* The quick growth of the (odd-numbered) count is evidence of the possibility of imposing extra structure to the red-black tree axioms.
* In the test, if significantly larger counts are queried, it may cause integer overflow.  To work around that, one can use long integers or custom integer structures in the templatized routine.

Heap.h
-----------

* Header file contains a C++ implementation of the **heap** data structure and Heap Sort.
* Heap Sort is a fast in-place sorting algorithm.  *In-place* means that only a constant amount of extra memory is required.
* The Heap Sort procedure takes O(*n* log(*n*)) time, where *n* is the size of the array to be sorted.
* This implementation has the bonus that it is templatized.
* Add this header file to a C++ project, and run programSamples::Heap<double>::test_heap() to see Heap Sort performed on an array of random numbers.

Trie.h
-----------

* Header file contains a C++ implementation of the **trie** data structure.
* Tries are efficient data structures for storing words.  They are characterized by simultaneously being deterministic finite automata (DFAs) and trees.  They are precisely those DFAs that recognize finite languages.
* Add this header file to a C++ project, and run programSamples::test_trie() to see several words inserted and searched.

HtmlDocumentation.zip
--------------
A zip file containing HTML documentation for the C++ programming samples.

**Java**
------

Documentator.zip
----------------
*Documentator* is a program written in Java that adds some automation to the process of writing source code documentation in C or C++ header files.

MergeSort.java
--------------
This Java class implements Merge Sort on Doubles.

JavaDocumentationHTML.zip
---------------------
A zip file containing HTML documentation for the Java programming samples.

**JavaScript**
--------------

ProgramSamples.html
-------------------
This HTML file contains references to JavaScript programming samples.

MergeSort.js
--------------
This file implements Merge Sort in JavaScript.

jsdoc.zip
---------
A zip file containing HTML documentation for the JavaScript programming samples.

GenerateJSDocumentation.sh
--------------------------
This Unix bash script automatically generates documentation using JSDoc from a JavaScript file in a given directory.  It needs to be edited to set the JSDoc directory and the directory containing the JavaScript file.

**Objective-C**
---------------

MergeSortIOS.h
--------------
* This file declares methods and variables in order to implement Merge Sort in Objective-C.
* **It needs to be renamed to MergeSort.h in order for it to work with the other Objective-C files.**
* These files can be included into an iOS project.

MergeSort.m
-----------
The implementation of the methods declared in MergeSortIOS.h.

ViewController.h
----------------
Contains method and variable declarations for a view controller for an iOS device.

ViewController.m
----------------
Implements the methods in ViewController.h.

ProgramSamplesIOS_HTML.zip
--------------------------
HTML documentation for the iOS project created using AppleDoc.