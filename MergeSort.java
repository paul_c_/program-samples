import java.util.List;
import java.util.ArrayList;
import java.util.Random;

/**
 * <h1>MergeSort</h1>
 *
 * @author Paul Cernea
 * @since 7/11/15.
 *
 * <br>Copyright (c) 2015 Paul Cernea. All rights reserved.</br>
 *
 * Implements the Merge Sort sorting algorithm on an array of Doubles.
 */
public class MergeSort
{
    private ArrayList<Double> aux_array;  /**< An auxiliary array for memory management during merging.*/
    private int aux_array_size;  /**< The size of the auxiliary array.*/

    /** Merges two sorted arrays.
     *
     * @param arr An array of Doubles containing the two sorted arrays
     * to be merged.  They are stored contiguously.
     * @param first_index The first index of the entire array.
     * Also, the first index of
     * the first sorted array to be merged.
     * @param half_index The first index of the second sorted array to be merged.
     * Also, half_index - 1 is the last index of the first sorted array to be
     * merged.
     * @param last_index The last index of the second sorted array to be merged.
     */
    private void merge(List<Double> arr, int first_index, int half_index, int last_index)
    {
        int length = last_index - first_index + 1;
            
        int i = first_index;  int j = half_index;

        for (int k = 0; k < length; k++)
        {
            // Boundary cases.

            if (i == half_index)
            {
                aux_array.set(k, arr.get(j));  j++;
                continue;
            }
            if (j == last_index + 1)
            {
                aux_array.set(k, arr.get(i));  i++;
                continue;
            }

            // Regular cases.

            Double a = arr.get(i);  Double b = arr.get(j);
            if (a < b)
            {
                aux_array.set(k, a);  i++;  continue;
            }
            //else
            {
                aux_array.set(k, b);  j++;  //continue;
            }
        }

        //Place contents of auxiliary array back in original.
            
        int l = first_index;
        for (int k = 0; k < length; k++)
        {
            arr.set(l, aux_array.get(k));  l++;
        }
    }
        
    /** Recursively sorts an array.
    *
    * The recursive sort proceeds as follows:  If the array has fewer than two
    * elements, there is nothing more to be done.  Otherwise, split the array
    * in half.  Recursively sort both halves.  Then perform a linear-time
    * merge to combine the arrays.
    * The entire procedure takes O(n * log(n)) time.
    * @param arr The array to be merge-sorted.
    * @param first_index The first index of the array.
    * @param last_index The last index of the array.
    */
    private void mergeSort(List<Double> arr, int first_index, int last_index)
    {
        int length = last_index - first_index + 1;
        if (length <= 1) {return;}
        int half_length = length / 2;
        int half_index = first_index + half_length;
        mergeSort(arr, first_index, half_index - 1);
        mergeSort(arr, half_index, last_index);
        merge(arr, first_index, half_index, last_index);
    }
        
    /** Initializes the auxiliary array,
    * and sets the auxiliary array size to 0.
    */
    public MergeSort()
    {
        aux_array = new ArrayList<Double>(0);  aux_array_size = 0;
    }

    /** Performs Merge Sort on an array of length arrayLength.
    *
    * Performs Merge Sort on an array of length arrayLength in O(n * log(n))
    * time, where n = arrayLength.  The method will terminate
    * if it cannot allocate the necessary O(n)
    * extra memory to perform the sort.  It uses the private method
    * MergeSort.mergeSort(List<Double>, int, int)
    * as a subroutine.
    * @param arr The array to be sorted.
    * @param arrayLength The length of the array to be sorted.
    */
    public void mergeSort(List<Double> arr, int arrayLength)
    {
        if (arrayLength > aux_array_size)
        {
            aux_array.ensureCapacity(arrayLength);
            
            int size_ = aux_array.size();
            
            for (int i = size_; i < arrayLength; i++)
            {
                aux_array.add(0.0);
            }
            aux_array_size = arrayLength;
        }

        mergeSort(arr, 0, arrayLength - 1);
    }
        
    /** Prints an array of length arrayLength.
    * @param arr The array to be printed.
    * @param arrayLength The length of the array to be printed.
    */
    public static void print(List<Double> arr, int arrayLength)
    {
        System.out.print("\n(");
        for (int i = 0; i < arrayLength; i++)
        {
            if (i > 0) {System.out.print(", ");}
            System.out.print(arr.get(i));
        }
        System.out.println(")\n");
    }

    /** Tests Merge Sort on an array of 70 pseudo-random numbers.
     *
     * The pseudo-random numbers have had the modulo operator applied to them to
     * make them less than 10,000.  This makes them visually easier to read.  Then
     * they are stored as doubles with 0.5 added to them.  The unsorted and sorted
     * arrays are printed.
     */
    public static void test_mergesort()
    {
        // Random doubles.
        {
            Random randNums = new Random();
            List<Double> myArray = new ArrayList<Double>();
            int arraySize = 70;
            for (int i = 0; i < arraySize; i++)
            {
                Double current = randNums.nextInt(10000) + 0.5;
                myArray.add(current);
            }
            
            System.out.println("\nUnsorted.\n");
            print(myArray, arraySize);
            
            MergeSort mySorter = new MergeSort();
            
            mySorter.mergeSort(myArray, arraySize);
            
            System.out.println("\nMerge sorted.\n");
            print(myArray, arraySize);
        }

    }

    public static void main(String[] args)
    {
        MergeSort.test_mergesort();
    }

};
