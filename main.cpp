//
//  main.cpp
//  ProgramSamples
//
//  Created by Paul Cernea on 7/8/15.
//  Copyright (c) 2015 Paul Cernea. All rights reserved.
//

#include "QueensProblem.h"
#include "MergeSort.h"
#include "RedBlackTree.h"
#include "CountRedBlack.h"
#include "Heap.h"
#include "Trie.h"

/** \mainpage Programming Samples
 * \author Paul Cernea
 *
 * \section intro_sec Introduction
 *
 * This project is a brief collection of some of my programming samples.
 * Implementations of some algorithms, data structures,
 * and solutions to problems are included,
 * along with corresponding tests.
 */

/** Main function of the program.  Relevant tests are run here.
 */
int main(int argc, const char * argv[])
{
    programSamples::testChessboard();
    programSamples::test_mergesort();
    programSamples::RedBlackTree<double>::test_red_black_tree();
    programSamples::Heap<double>::test_heap();
    programSamples::test_trie();
    programSamples::testRedBlackCount();
    return 0;
}
