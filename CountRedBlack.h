//
//  CountRedBlack.h
//  AlgorithmsStructures
//
//  Created by Paul Cernea on 7/31/15.
//  Copyright (c) 2015 Paul Cernea. All rights reserved.
//

#ifndef AlgorithmsStructures_CountRedBlack_h
#define AlgorithmsStructures_CountRedBlack_h

#include <iostream>
#include <string>

/*! \file CountRedBlack.h
 \brief Counts the number of red-black trees with a given number of nodes.
 */

namespace programSamples
{
    /** \brief Counts the number of red-black trees with a given number of nodes.
     *
     * @param NUM The type of number used for counting.  An integer could suffice,
     * but it might also be desirable to use long integers or arbitrary-precision
     * integers.
     */
    template <class NUM> class CountRedBlack
    {
        /** \brief A table for storing previously computed quantities to aid in
         * the calculation.
         *
         * <code>RBCount[(i * (i + 1)) / 2 + j]</code> represents the
         * number of red-black trees with <code>i</code> nodes and a black-height
         * of <code>j</code>.
         */
        NUM * RBCount;
        /** \brief The largest number N for which a count has been queried.
         */
        int largest_N;
        
        /** \brief Private method to get already calculated counts.
         */
        NUM R_(const int & i, const int & j) const
        {
            if (j > i) {return 0;}
            return RBCount[(i * (i + 1)) / 2 + j];
        }
    public:
        CountRedBlack() : RBCount(NULL), largest_N(0) {}
        ~CountRedBlack() {if (RBCount != NULL) {free(RBCount);}}
        /** \brief Counts the number of red-black trees of order N.
         *
         * @param N The number for which a count of red-black trees is desired.
         * @return The number of red-black trees with N nodes.
         *
         * Trees are counted as different if they are not identical, even if there
         * is a symmetry that takes one to the other.
         */
        NUM getCount(const int & N)
        {
            try
            {
                if (RBCount == NULL)
                {
                    RBCount = (NUM *) calloc(1, sizeof(NUM));
                    RBCount[0] = 0;
                }
                
                if (N > largest_N)
                {
                    int newSize = (N + 1);
                    newSize = (newSize * (newSize + 1)) / 2;
                    RBCount = (NUM *) realloc(RBCount, newSize * sizeof(NUM));
                    for (int i = largest_N + 1; i <= N; i++)
                    {
                        for (int j = 0; j <= i; j++)
                        {
                            NUM * current = RBCount + ((i * (i + 1)) / 2 + j);
                            *current = 0;
                            if ((j == 0) && (i > 0)) {continue;}
                            if (j == 1)
                            {
                                if (i == 1) {*current = 1;}
                                continue;
                            }
                            for (int k = 0; k < i; k++)
                            {
                                *current = *current + R_(k, j - 1) * R_(i - 1 - k, j - 1);
                            }
                            for (int k = 0; k < i - 1; k++)
                            {
                                for (int l = 0; l <= i - 2 - k; l++)
                                {
                                    *current = *current + 2 * R_(k, j - 1) * R_(l, j - 1) * R_(i - 2 - k - l, j - 1);
                                }
                            }
                            for (int k = 0; k < i - 2; k++)
                            {
                                for (int l = 0; l <= i - 3 - k; l++)
                                {
                                    for (int m = 0; m <= i - 3 - k - l; m++)
                                    {
                                        *current = *current + R_(k, j - 1) * R_(l, j - 1) * R_(m, j - 1) * R_(i - 3 - k - l - m, j - 1);
                                    }
                                }
                            }
                        }
                    }
                    
                    largest_N = N;
                }
                
                if (RBCount == NULL) {throw "\nError.  Out of memory.\n";}
            }
            catch (const char * msg)
            {
                std::cerr << msg << std::endl;  return 0;
            }
            NUM answer = 0;
            
            for (int i = 0; i <= N; i++)
            {
                answer = answer + R_(N, i);
            }
            return answer;
        }
    };
    
    void testRedBlackCount()
    {
        CountRedBlack<int> countOperator;
        
        std::cout << "\n***************\nHere are some red-black tree counts.";
        std::cout << "\nNote that we assume all leaves to be black and a root";
        std::cout << "\nto exist.  So for instance there are no trees with an";
        std::cout << "\neven number of nodes.  We could store an even number of";
        std::cout << "\nnodes, however, by taking a larger tree.  We would then";
        std::cout << "\nneed to have some extra black NULL leaf nodes.";
        std::cout << "\nThe fast growth is evidence that there is room to add.";
        std::cout << "\nextra structure to the red-black tree axioms.";
        std::cout << "\n***************\n";
        
        int numCalculations = 55;
        for (int i = 0; i < numCalculations; i++)
        {
            countOperator.getCount(i); //Activates recurrence.
        }
        for (int i = 0; i < numCalculations; i++)
        {
            std::cout << "\nNumber of red-black trees with " << i << " nodes: ";
            std::cout << countOperator.getCount(i);
            std::cout << ".";
        }
        std::cout << "\n";
    }
}

#endif
