//
//  Heap.h
//  AlgorithmsStructures
//
//  Created by Paul Cernea on 7/7/15.
//  Copyright (c) 2015 Paul Cernea. All rights reserved.
//

#ifndef AlgorithmsStructures_Heap_h
#define AlgorithmsStructures_Heap_h
#include <cstdlib>   // memory, rand
#include <iostream>  // output

/*! \file Heap.h
 \brief Implements the Heap data structure.
 */

namespace programSamples
{
    /** \brief Instantiate with a given array in order to give the array a heap
     * structure.
     *
     * @param NUM A template argument representing the type of object that will be compared.  If NUM is a custom type, the less-than operator < must
     * be overloaded.
     */
    template <class NUM> class Heap
    {
        NUM * base_array;  /**< The array on which the heap structure is built.*/
        int array_size;  /**< The size of the array.*/
        int heap_size; /**< The size of the heap.*/
        
        /** \brief Given a child index, returns the parent index.
         *
         * A self-contained routine is desirable because the indexing scheme
         * involves several shifts.
         * @param i The child index.
         */
        static int get_parent_index(const int & i)
        {
            int ii = i + 1; //Indices must start at 1, not 0.
            int parent_index = ii / 2;
            return parent_index - 1; //Shift index back.
        }
        
        /** \brief Given a parent index, returns its left child index.
         *
         * A self-contained routine is desirable because the indexing scheme
         * involves several shifts.
         * @param i The parent index.
         */
        static int get_left_child(const int & i)
        {
            int ii = i + 1; //Indices must start at 1, not 0.
            int child = 2 * ii;
            return child - 1; //Shift index back.
        }
        
        /** \brief Given a parent index, returns its right child index.
         *
         * A self-contained routine is desirable because the indexing scheme
         * involves several shifts.
         * @param i The parent index.
         */
        static int get_right_child(const int & i)
        {
            int ii = i + 1; //Indices must start at 1, not 0.
            int child = 2 * ii + 1;
            return child - 1; //Shift index back.
        }
        
        /** \brief Maintains the max-heap property rooted at a given index, given
         * that the property holds for the children of the index.
         *
         * This routine assumes that the max-heap property already holds at
         * the left and right children of the given index.
         *
         * The running time of this routine is O(log(n)) where n is the size of
         * the heap.  It requires a constant amount of extra space.
         *
         * @param i The index at which the current heap is based.
         */
        void maintainMaxHeap(const int & i)
        {
            int current = i;
            while (true)
            {
                int left_index  = get_left_child (current);
                int right_index = get_right_child(current);
                
                // Determine the index of the largest element.
                
                int max_index = 0;
                if ((left_index < heap_size) &&
                    (base_array[current] < base_array[left_index]))
                {
                    max_index = left_index;
                }
                else {max_index = current;}
                
                if ((right_index < heap_size) &&
                    (base_array[max_index] < base_array[right_index]))
                {
                    max_index = right_index;
                }
                
                if (current != max_index)
                {
                    // Swap the entries at max_index and current.
                    NUM temp = base_array[current];
                    base_array[current] = base_array[max_index];
                    base_array[max_index] = temp;
                    
                    // Replace current with the max_index and reiterate.
                    current = max_index;
                    continue;
                }
                
                return;
            }
        }
        
    public:
        
        /** \brief Initializes the base array to NULL
         * and the sizes to 0.
         */
        Heap() : base_array(NULL), array_size(0), heap_size(0) {}
        
        /** \brief Initializes the base array and its size to a given array
         *
         * @param arr The array that will become the base array.  Note that no new
         * memory is allocated, as all basic operations are in-place.
         * @param siz The size of the array.
         */
        Heap(NUM * arr, const int & siz) : base_array(arr), array_size(siz), heap_size(0) {}
        
        /** \brief Possibly rearranges elements in the base array, ensuring that
         * the array satisfies the max-heap property.
         *
         * The running time for this routine is O(n) where n is the size of the
         * array.  It requires a constant extra amount of space.
         */
        void buildMaxHeap()
        {
            heap_size = array_size;
            for (int i = get_parent_index(heap_size - 1); i >= 0; i--)
            {
                maintainMaxHeap(i);
            }
        }
        
        /** \brief Verifies whether the current heap indeed satisfies the
         * max-heap properties.
         *
         * @return <code>true</code> if the current heap satisfies the max-heap
         * properties, <code>false</code> otherwise.
         */
        bool isMaxHeap() const
        {
            for (int i = 1; i < heap_size; i++)
            {
                if (get_left_child(i) < heap_size)
                {
                    if (base_array[i] < base_array[get_left_child(i)])
                    {
                        return false;
                    }
                }
                if (get_right_child(i) < heap_size)
                {
                    if (base_array[i] < base_array[get_right_child(i)])
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        
        /** \brief Uses a heap to sort an array in place.
         *
         * This routine runs in O(n * log(n)) time and O(1) extra space.
         * @param array The array to be printed.
         * @param arrayLength The length of the array to be printed.
         */
        static void heapSort(NUM * arr, int arrayLength)
        {
            Heap<NUM> hh(arr, arrayLength);
            hh.buildMaxHeap();
            //if (!hh.isMaxHeap()) {std::cout << "\nNot max heap.";  return;}
            for (int i = arrayLength - 1; i >= 1; i--)
            {
                // Swap last element in current heap with first element.
                {
                    NUM temp = hh.base_array[0];
                    hh.base_array[0] = hh.base_array[i];
                    hh.base_array[i] = temp;
                }
                
                hh.heap_size--;
                hh.maintainMaxHeap(0);
                //if (!hh.isMaxHeap()) {std::cout << "\nNot max heap.";  return;}
            }
        }
        
        /** \brief Confirms that an array is sorted.
         *
         * This routine runs in O(n) time and O(1) extra space.
         * @param array The array that is supposed to be sorted.
         * @param arrayLength The length of the array.
         * @return <code>true</code> if the current array is sorted,
         * <code>false</code> otherwise.
         */
        static bool isSorted(NUM * arr, int arrayLength)
        {
            for (int i = 0; i < arrayLength; i++)
            {
                if (i == 0) {continue;}
                if (arr[i] < arr[i - 1]) {return false;}
            }
            return true;
        }
        
        /** \brief Prints an array of length arrayLength.
         *
         * @param array The array to be printed.
         * @param arrayLength The length of the array to be printed.
         */
        static void print(NUM * array, int arrayLength)
        {
            std::cout << "\n(";
            for (int i = 0; i < arrayLength; i++)
            {
                if (i > 0) {std::cout << ", ";}
                std::cout << array[i];
            }
            std::cout << ")\n";
        }
        
        /** \brief Tests heapSort on an array of 70 pseudo-random numbers.
         *
         * The pseudo-random numbers have had the modulo operator applied to them to
         * make them less than 10,000.  This makes them visually easier to read.  Then
         * they are stored as doubles with 0.5 added to them.  The unsorted and sorted
         * arrays are printed.
         */
        static void test_heap()
        {
            srand((unsigned) time(0));
            const int arraySize = 70;
            double myArray[arraySize];
            for (int i = 0; i < arraySize; i++)
            {
                int current = rand() % 10000;
                myArray[i] = current + 0.5;
            }
            
            std::cout << "\nUnsorted.\n";
            Heap<double>::print(myArray, arraySize);
            
            Heap<double>::heapSort(myArray, arraySize);
            
            if (!Heap<double>::isSorted(myArray, arraySize))
            {
                std::cout << "\nHeap sort failed.\n";
            }
            
            std::cout << "\nHeap sorted.\n";
            Heap<double>::print(myArray, arraySize);
            
            std::cout << "\nHeap sort succeeded.\n";
        }
    };

}

#endif
