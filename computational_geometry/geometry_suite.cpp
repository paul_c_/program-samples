#include "gl_impl.h"
#include "Testing/Testing.h"
#include <string>
using namespace ComputationalGeometry;

int window_id = -1;
int numRandomPoints = 1000;
std::vector<Point2> point_array;
std::vector<Point2> convex_hull;
std::vector<Segment2> edges;
bool testing = false;
bool slow = false;

int main(int argc, char **argv)
{
    if (argc >= 3)
    {
        std::string input = argv[2];
        if ((input.compare("--slow") == 0) || (input.compare("-s") == 0))
            slow = true;
    }
    
	if (argc >= 2)
	{
        bool isInt = true;
        std::string input = argv[1];
        int numPoints = 0;
        try
        {
            numPoints = std::stoi(input);
        }
        catch (...)
        {
            isInt = false;
        }
		if (isInt) { numRandomPoints = numPoints; }
        if ((input.compare("--testing") == 0) || (input.compare("-t") == 0))
            testing = true;
	}
	
    if (testing)
    {
        basicTest();
        return 0;
    }
	
	srand(time(NULL));
	
	initialize_glut(&argc, argv);

	glutMainLoop();
	
	return 0;
}
