/*  Copyright Paul Cernea, May 30, 2017.
All Rights Reserved.*/

#ifndef GL_IMPL_H
#define GL_IMPL_H

#include "Primitives/Segment2.h"

#define WINDOW_WIDTH  (1024)
#define WINDOW_HEIGHT (1024)

extern int window_id;
extern int numRandomPoints;

extern void recompute();
extern void initialize_glut(int * argc_ptr, char **argv);
extern void keyboard(unsigned char key, int x, int y);

void mouse(int button, int state, int x, int y);
void render();

extern std::vector<ComputationalGeometry::Point2> point_array;
extern std::vector<ComputationalGeometry::Point2> convex_hull;
extern std::vector<ComputationalGeometry::Segment2> edges;
extern bool slow;

#endif // GL_IMPL_H
