* Run `bash compile_and_run_geometry.sh` to run the computational geometry suite.  By default, 1000 points are generated.
* Run `bash compile_and_run_geometry.sh <number-of-points>` to run the computational geometry suite with your desired number of points.  For instance `bash compile_and_run_geometry.sh 200` would run the program with 200 points.
* Click the window to generate a new point set.
* The convex hull is automatically generated.
* Press **Q** or **ESC** to exit.