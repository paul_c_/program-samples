/*  Copyright Paul Cernea, May 25, 2019.
All Rights Reserved.*/

#include "gl_impl.h"
#include "Primitives/Triangle.h"
using namespace ComputationalGeometry;

unsigned char option = 'c'; // Convex hull.

void keyboard(unsigned char key, int x, int y)
{
    if ((key == 27) //Esc
        || (key == 'q') || (key == 'Q'))
    {
        std::cout << "\nDone.\n";
        glutDestroyWindow(window_id);
        exit (0);
    }
    
    if ((key == 'c') || (key == 'C'))
    {
        std::cout << "\nDrawing convex hull (Graham scan).";
        if (option == 'c') { recompute(); }
        else
        {
            option = 'c';
            Point2::grahamScan(convex_hull, point_array);
        }
        glutPostRedisplay();
        return;
    }
    
    if ((key == 'k') || (key == 'K'))
    {
        std::cout << "\nDrawing convex hull (quick hull).";
        if (option == 'k') { recompute(); }
        else
        {
            option = 'k';
            Point2::quickHull(edges, point_array);
        }
        glutPostRedisplay();
        return;
    }
    
    if ((key == 't') || (key == 'T'))
    {
        std::cout << "\nDrawing Delaunay triangulation.";
        if (option == 't') { recompute(); }
        else
        {
            option = 't';
            Point2::delaunayTriangulation(edges, point_array);
        }
        glutPostRedisplay();
        return;
    }
    
    glutPostRedisplay();
}

void recompute()
{
    // Generate random points for display.
    Point2::generate_random_points(point_array, numRandomPoints);
    if (option == 'c')
    {
        // Compute convex hull of the 2d points with Graham Scan.
        Point2::grahamScan(convex_hull, point_array);
        return;
    }
    if (option == 'k')
    {
        // Compute convex hull of the 2d points with Quick Hull.
        Point2::quickHull(edges, point_array);
        return;
    }
    if (option == 't')
    {
        // Compute Delaunay triangulation of the 2d points.
        Point2::delaunayTriangulation(edges, point_array);
        return;
    }
}

void initialize_glut(int * argc_ptr, char **argv)
{
    // Initialize GLUT and create a window.
    glutInit(argc_ptr, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(-1, -1);
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    
    window_id = glutCreateWindow("Computational Geometry - Paul Cernea - Press 'q' to exit.");
    
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    
    glutSetCursor(GLUT_CURSOR_INFO);
    
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutDisplayFunc(render);
    
    recompute();
}

void mouse(int button, int state, int x, int y)
{
    if((button == GLUT_LEFT_BUTTON) && (state == GLUT_UP))
    {
        recompute();
    }
    
    glutPostRedisplay();
}

static void draw(const Point2& P)
{
    glVertex2f(P.x, P.y);
}

void render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glColor3f(0.0f, 0.0f, 0.0f);
    
    glPointSize(3.0f);
    
    glBegin(GL_POINTS);
    
    {
        int siz_point_array = point_array.size();
        
        for (int i = 0; i < siz_point_array; i++)
        {
            Point2 P = point_array[i];
            
            glVertex2f(P.x, P.y);
        }
    }
    
    glEnd();

    if (option == 'c')
    {
        glColor3f(1.0f, 0.0f, 0.0f);
        glBegin(GL_LINE_LOOP);
        int siz_point_array = convex_hull.size();
        
        for (int i = 0; i < siz_point_array; i++)
        {
            Point2 P = convex_hull[i];
            draw(P);
        }
        
        glEnd();
    }
    else if (option == 'k')
    {
        glColor3f(0.0f, 0.5f, 0.0f);
        
        int siz_edges = edges.size();
        
        for (int i = 0; i < siz_edges; i++)
        {
            glBegin(GL_LINE_LOOP);
            Point2 P = edges[i].p;
            Point2 Q = edges[i].q;
            draw(P);
            draw(Q);
            draw(P);
            glEnd();
        }
    }
    else if (option == 't')
    {
        glColor3f(0.0f, 0.0f, 1.0f);
        
        int siz_edges = edges.size();
        
        for (int i = 0; i < siz_edges; i++)
        {
            glBegin(GL_LINE_LOOP);
            Point2 P = edges[i].p;
            Point2 Q = edges[i].q;
            draw(P);
            draw(Q);
            draw(P);
            glEnd();
        }
    }
    
    glutSwapBuffers();
}
