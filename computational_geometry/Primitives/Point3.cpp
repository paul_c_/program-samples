/*  Copyright Paul Cernea, May 25, 2019.
All Rights Reserved.*/

#include "Point3.h"
#include "Plane3.h"

namespace ComputationalGeometry
{
    Num Point3::absVal(const Num& val)
    {
        if (0 < val) { return val; }
        return -val;
    }
    
    bool Point3::operator<(const Point3& q) const
    {
        if (x > q.x) {return false;}
        if (x < q.x) {return  true;}
        if (y > q.y) {return false;}
        if (y < q.y) {return  true;}
        if (this->get_dimension() <= 2) {return false;}
        if (z > q.z) {return false;}
        if (z < q.z) {return  true;}
        return false;
    }
    
    bool Point3::operator==(const Point3& q) const
    {
        if (*this < q) { return false; }
        if (q < *this) { return false; }
        return true;
    }
    
    void Point3::print(const std::string& prequel) const
    {
        std::cout << prequel << "(" << x << ", " << y;
        if (this->get_dimension() > 2) {std::cout << ", " << z;}
        std::cout << ")";
    }
    
    Num Point3::sq_distance(const Point3& P, const Point3& Q)
    {
        Num answer = 0;
        Num dt = (P.x - Q.x);
        answer = answer + dt * dt;
        dt = (P.y - Q.y);
        answer = answer + dt * dt;
        if ((P.get_dimension() > 2) || (Q.get_dimension() > 2))
        {
            dt = (P.z - Q.z);
            answer = answer + dt * dt;
        }
        return answer;
    }
    
    Point3 Point3::cross(const Point3& rhs) const
    {
        Point3 prod;
        prod.x = y * rhs.z - z * rhs.y;
        prod.y = z * rhs.x - x * rhs.z;
        prod.z = x * rhs.y - y * rhs.x;
        return prod;
    }

    Point3 Point3::operator+(const Point3& rhs) const
    {
        Point3 sum = *this;
        sum.x = sum.x + rhs.x;
        sum.y = sum.y + rhs.y;
        sum.z = sum.z + rhs.z;
        return sum;
    }
    
    Point3 Point3::operator-(const Point3& rhs) const
    {
        Point3 diff = *this;
        diff.x = diff.x - rhs.x;
        diff.y = diff.y - rhs.y;
        diff.z = diff.z - rhs.z;
        return diff;
    }
    
    std::ostream& operator<<(std::ostream& str, const Point3& pt)
    {
        auto& strOut = str;
        strOut << "(" << pt.x << ", " << pt.y;
        if (pt.get_dimension() > 2) { strOut << ", " << pt.z; }
        strOut << ")";
        return strOut;
    }
    
    /** Find points from pts that lie north of triangle (aa, bb, cc)
     *  and lie in the convex hull.
     */
    static void subHull(const std::vector<Point3>& pts, const Point3& aa, const Point3& bb, const Point3& cc, std::vector<Plane3>& hullFacets)
    {
        int ptsSize = (int) pts.size();
        if (ptsSize == 0) { hullFacets.push_back(Plane3(aa, bb, cc));  return; }
        Num maxSqDist = 0;
        Plane3 plan(aa,bb, cc);
        int i = -1;
        int bestIndex = 0;
        Point3 farPoint;
        for (const auto& pt : pts)
        {
            ++i;
            Point3 temp;
            Num sqDist = plan.sqDistToPlane(pt, temp);
            if ((i == 0) || (sqDist > maxSqDist))
            {
                bestIndex = i;
                farPoint = pt;
                maxSqDist = sqDist;
            }
        }
        //hull.push_back(farPoint); // Should be within aa, bb, and cc.
        Plane3 plan1(aa, bb, farPoint);
        Plane3 plan2(bb, cc, farPoint);
        Plane3 plan3(cc, aa, farPoint);
        std::vector<Point3> north1;
        std::vector<Point3> north2;
        std::vector<Point3> north3;
        i = 0;
        for (const auto& pt : pts)
        {
            ++i;
            if ((i == bestIndex)) { continue; }
            using Inter = Plane3::Intersection;
            const Inter side1 = plan1.intersects(pt);
            const Inter side2 = plan2.intersects(pt);
            const Inter side3 = plan3.intersects(pt);
            if (side1 == Inter::North) { north1.push_back(pt); }
            if (side2 == Inter::North) { north2.push_back(pt); }
            if (side3 == Inter::North) { north3.push_back(pt); }
        }
        subHull(north1, aa, bb, farPoint, hullFacets);
        subHull(north2, bb, cc, farPoint, hullFacets);
        subHull(north2, cc, aa, farPoint, hullFacets);
    }
    
    void Point3::quickHull(std::vector<Plane3>& hullFacets, const std::vector<Point3>& points, const HullType ht)
    {
        hullFacets.resize(0);
        int ptsSize = (int) points.size();
        if (ptsSize <= 4)
        {
            //hull = points;
            if (ptsSize == 4)
            {
                hullFacets.push_back(Plane3(points[0], points[1], points[2]));
                hullFacets.push_back(Plane3(points[1], points[2], points[3]));
                hullFacets.push_back(Plane3(points[2], points[3], points[0]));
                hullFacets.push_back(Plane3(points[3], points[0], points[1]));
                return;
            }
            if (ptsSize == 3)
            {
                hullFacets.push_back(Plane3(points[0], points[1], points[2]));
                return;
            }
            if (ptsSize == 2)
            {
                hullFacets.push_back(Plane3(points[0], points[1], points[0]));
                return;
            }
            hullFacets.push_back(Plane3(points[0], points[0], points[0]));
            return;
        }
        //hull.resize(0);
        Plane3 midplane;
        {
            Point3 minPt, maxPt, maxY;
            int minIndex = 0, maxIndex = 0, maxIndexY = 0;
            int i  = -1;
            for (const auto& pt : points)
            {
                ++i;
                if (i == 0)
                {
                    minPt = maxPt = pt;
                    continue;
                }
                if (pt.x < minPt.x) { minIndex = i;  minPt = pt; }
                if (pt.x > maxPt.x) { maxIndex = i;  maxPt = pt; }
                if (pt.y >  maxY.y) { maxIndexY = i;  maxY = pt; }
            }
            if (minIndex == maxIndex)
            {
                //hull = points;
                return;
            }
            //hull.push_back(minPt);
            //hull.push_back(maxPt);
            //hull.push_back(maxY);
            midplane = Plane3(minPt, maxPt, maxY);
            
            i = -1;
            std::vector<Point3> north;
            std::vector<Point3> south;
            for (const auto& pt : points)
            {
                ++i;
                if ((i == minIndex) || (i == maxIndex) || (i == maxIndexY)) { continue; }
                using Inter = Plane3::Intersection;
                const Inter side = midplane.intersects(pt);
                if (side == Inter::North) { north.push_back(pt); }
                if (side == Inter::South) { south.push_back(pt); }
            }
            if (ht != HullType::Lower) { subHull(north, minPt, maxPt, maxY, hullFacets); }
            if (ht != HullType::Upper) { subHull(south, maxY, maxPt, minPt, hullFacets); }
        }
    }
}
