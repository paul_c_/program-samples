/*  Copyright Paul Cernea, June 20, 2019.
All Rights Reserved.*/

#include "BoundingBox.h"

namespace ComputationalGeometry
{
    BoundingBox Point3::getExtremePoints(const std::vector<Point3>& pts)
    {
        BoundingBox box;
        int i  = -1;
        for (const auto& pt : pts)
        {
            ++i;
            if (i == 0) { box = BoundingBox(i, pt); continue; }
            if (pt.x < box.minX.x) { box.indexMinX = i;  box.minX = pt; }
            else if (pt.x > box.maxX.x) { box.indexMaxX = i;  box.maxX = pt; }
            if (pt.y < box.minY.y) { box.indexMinY = i;  box.minY = pt; }
            else if (pt.y > box.maxY.y) { box.indexMaxY = i;  box.maxY = pt; }
            if (pt.z < box.minZ.z) { box.indexMinZ = i;  box.minZ = pt; }
            else if (pt.z > box.maxZ.z) { box.indexMaxZ = i;  box.maxZ = pt; }
        }
        return box;
    }
}
