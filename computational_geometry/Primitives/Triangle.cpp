/*  Copyright Paul Cernea, May 25, 2019.
All Rights Reserved.*/

#include "Triangle.h"

namespace ComputationalGeometry
{
    Triangle::Intersection Triangle::intersects(const Point2& pt) const
    {
        Point2 test = Point2::moveToUnitRightTriangle(a, b, c, pt);
        if (test.x < 0 || test.x > 1 || test.y < 0 || test.y > 1) { return Exterior; }
        test = Point2::moveToUnitRightTriangle(b, c, a, pt);
        if (test.x < 0 || test.x > 1 || test.y < 0 || test.y > 1) { return Exterior; }
        test = Point2::moveToUnitRightTriangle(c, a, b, pt);
        if (test.x < 0 || test.x > 1 || test.y < 0 || test.y > 1) { return Exterior; }
        
        // TODO: Improve boundary criteria.
        
        return Interior;
    }
    
    void Triangle::basicTriangulate(std::vector<Segment2>& edges, const std::vector<Point2>& points, const unsigned& N)
    {
        int sizPoints = (int) points.size();
        edges = std::vector<Segment2>();
        std::vector<Triangle> tris = std::vector<Triangle>();
        if (sizPoints == 0) {}
        else if (sizPoints == 1)
        {
            edges.push_back(Segment2(points[0], points[0]));
        }
        else
        {
            edges.push_back(Segment2(points[0], points[1]));
        }
        if (sizPoints < 3)
        {
            std::cout << "\nNeed at least 3 points for a triangulation.";
            return;
        }
        edges.push_back(Segment2(points[1], points[2]));
        edges.push_back(Segment2(points[2], points[0]));
        tris.push_back(Triangle(points[0], points[1], points[2]));
        std::vector<Point2> pastPoints;
        pastPoints.push_back(points[0]);
        pastPoints.push_back(points[1]);
        pastPoints.push_back(points[2]);
        for (int i = 3; i < sizPoints; ++i)
        {
            bool intersectsExt = true;
            Point2 pt = points[i];
            for (auto it = tris.begin(); it != tris.end(); ++it)
            {
                auto& tri = *it;
                if (tri.intersects(pt) == Triangle::Interior)
                {
                    Triangle triTmp = tri;
                    tris.erase(it);
                    edges.push_back(Segment2(pt, triTmp.a));
                    edges.push_back(Segment2(pt, triTmp.b));
                    edges.push_back(Segment2(pt, triTmp.c));
                    tris.push_back(Triangle(pt, triTmp.a, triTmp.b));
                    tris.push_back(Triangle(pt, triTmp.b, triTmp.c));
                    tris.push_back(Triangle(pt, triTmp.c, triTmp.a));
                    intersectsExt = false;
                    break;
                }
            }
            if (intersectsExt)
            {
                std::vector<Point2> newHull;
                Point2::grahamScan(newHull, pastPoints);
                // Fill this in.
            }
            pastPoints.push_back(pt);
        }
    }
    
    Num Triangle::signedArea() const
    {
        return ((b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y)) * 0.5;
    }
    
    Num Triangle::area() const
    {
        return Point3::absVal(signedArea());
    }
}
