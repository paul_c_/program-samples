/*  Copyright Paul Cernea, May 30, 2017.
All Rights Reserved.*/

#ifndef POINT_2_H
#define POINT_2_H

#include "Point3.h"

#include "BoundingBox.h"

namespace ComputationalGeometry
{
    class Segment2;
    
    class Point2 : public Point3
    {
    public:
        Point2() : Point3(0, 0, 0) {}
        Point2(const Num& xx, const Num& yy) : Point3(xx, yy, 0) {}
        Point2(const Point3& rhs) : Point3(rhs.x, rhs.y, 0) {}
        
        int get_dimension() const override {return 2;}
        static void generate_random_points(std::vector<Point2>& container, const unsigned& N);
        /** Graham scan runs in O(n * log(n)) time where n is the number of points.
         */
        static void grahamScan(std::vector<Point2>& hull, const std::vector<Point2>& points);
        /** Quick hull runs worst case in O(n^2) time where n is the number of points.
         */
        static void quickHull(std::vector<Segment2>& hullEdges, const std::vector<Point2>& points, const HullType ht = HullType::Full);
        /** Maximizes minimum angle in triangulation.
         */
        static void delaunayTriangulation(std::vector<Segment2>& triEdges, const std::vector<Point2>& points);
        /** This map, takes pt to pt' such that a is taken to 0, b is taken to (0,1) and c is taken to (1,0).
         */
        static Point2 moveToUnitRightTriangle(const Point2& a, const Point2& b, const Point2& c, const Point2& pt);
        
        static BoundingBox getExtremePoints(const std::vector<Point2>& pts);
        
        template <class Container> static Num min_sq_distance(Container& arr, Point2& min_1, Point2& min_2)
        {
            Num min = 0;
            
            unsigned arr_count = (unsigned) arr.size();
            
            if (arr_count == 0) {return 0;}
            if (arr_count == 1) {min_1 = *(arr.begin());  min_2 = *(arr.begin());  return 0;}
            if (arr_count == 2)
            {
                typename Container::iterator it = arr.begin();
                min_1 = *it;  ++it;  min_2 = *it;
                return min_1.squareDistance(min_2);
            }
            if (arr_count == 3)
            {
                typename Container::iterator it = arr.begin();
                Point2 a = *it;  ++it;  Point2 b = *it;
                Num min_ = a.squareDistance(b);
                min_1 = a;  min_2 = b;
                ++it;
                Num candidate = a.squareDistance(*it);
                if (candidate < min_)
                {
                    min_ = candidate;  /*min_1 = a;*/  min_2 = *it;
                }
                candidate = b.squareDistance(*it);
                if (candidate < min_)
                {
                    min_ = candidate;  min_1 = *it;  min_2 = b;
                }
                return min_;
            }
            
            std::vector<Point2 > arr_;
            for (typename Container::iterator it = arr.begin(); it != arr.end(); ++it)
            {
                arr_.push_back(*it);
            }
            
            std::sort(arr_.begin(), arr_.end());
            
            min = min_sq_distance_(arr_, min_1, min_2);
            
            return min;
        }
        
        Point2 operator+(const Point2& rhs) const;
        Point2 operator-(const Point2& rhs) const;
        Point2 operator*(const Num& scal) const;
        Num dot(const Point2& rhs) const;
        Num squareMag() const;
        Num squareDistance(const Point2& rhs) const;
 
    private:
        friend std::vector<Point2>& operator+=(std::vector<Point2>& pts, const Point2& pt)
        {
            for (auto& point : pts) { point = point + pt; }  return pts;
        }
        friend std::vector<Point2>& operator-=(std::vector<Point2>& pts, const Point2& pt)
        {
            for (auto& point : pts) { point = point - pt; }  return pts;
        }
        
        template <class Container> static Num min_sq_distance_(Container& arr, Point2& min_1, Point2& min_2)
        {
            Num min = 0;
            
            unsigned arr_count = (unsigned) arr.size();
            
            // These cases (where arr_count is 0 or 1) should never happen in the private method.
            //if (arr_count == 0) {return 0;}
            //if (arr_count == 1) {min_1 = *(arr.begin());  min_2 = *(arr.begin());  return 0;}
            if (arr_count == 2)
            {
                typename Container::iterator it = arr.begin();
                min_1 = *it;  ++it;  min_2 = *it;
                return min_1.squareDistance(min_2);
            }
            if (arr_count == 3)
            {
                typename Container::iterator it = arr.begin();
                Point2 a = *it;  ++it;  Point2 b = *it;
                Num min_ = a.squareDistance(b);
                min_1 = a;  min_2 = b;
                ++it;
                Num candidate = a.squareDistance(*it);
                if (candidate < min_)
                {
                    min_ = candidate;  /*min_1 = a;*/  min_2 = *it;
                }
                candidate = b.squareDistance(*it);
                if (candidate < min_)
                {
                    min_ = candidate;  min_1 = *it;  min_2 = b;
                }
                return min_;
            }
            
            unsigned half_arr_count = arr_count / 2;
            unsigned remaining_arr_count = arr_count - half_arr_count;
            
            Container arr_1, arr_2;
            Point2 left_1, left_2, right_1, right_2;
            Num min_L, min_R;
            
            {
                typename Container::iterator it = arr.begin();
                for (int i = 0; i < half_arr_count; i++)
                {
                    arr_1.push_back(*it);  ++it;
                }
                
                for (int i = 0; i < remaining_arr_count; i++)
                {
                    arr_2.push_back(*it);  ++it;
                }
            }
            
            min_L = min_sq_distance_(arr_1, left_1,  left_2);
            min_R = min_sq_distance_(arr_2, right_1, right_2);
            
            // needs improvement
            
            if (min_L < min_R)
            {
                min = min_L;
                min_1 = left_1;
                min_2 = left_2;
            }
            else
            {
                min = min_R;
                min_1 = right_1;
                min_2 = right_2;
            }
            
            return min;
        }
    };
}

#endif // POINT_2_H
