/*  Copyright Paul Cernea, May 25, 2019.
All Rights Reserved.*/

#include "Point2.h"
#include "Plane3.h"
#include "Triangle.h"

namespace ComputationalGeometry
{
    void Point2::generate_random_points(std::vector<Point2>& container, const unsigned& N)
    {
        //double WW = WINDOW_WIDTH  - 1;  if(WW <= 0) {WW = 2;}
        //double HH = WINDOW_HEIGHT - 1;  if(HH <= 0) {HH = 2;}
        double randMax = RAND_MAX;
        
        container.resize(0);
        std::set<Point2 > initial_set; //Using a set initially ensures unique points and allows automatic sorting.
        
        for (int i = 0; i < N; ++i)
        {
            Num xx = (rand() * 1.8 / randMax) - 0.9;
            Num yy = (rand() * 1.8 / randMax) - 0.9;
            Point2 P(xx, yy);
            initial_set.insert(P);
        }
        
        for (typename std::set<Point2 >::iterator it = initial_set.begin(); it != initial_set.end(); ++it)
        {
            container.push_back(*it);
        }
    }
    
    class AngleCompare
    {
        Point2 origin;
        static int assignQuadrant(const Point2& pt)
        {
            if (pt.x < 0) { return (pt.y > 0) ? 4 : 1; }
            return (pt.y > 0) ? 3 : 2;
        }
        
    public:
        AngleCompare(const Point2& O) : origin(O) {}
        bool operator()(const Point2& lhs, const Point2& rhs) const
        {
            int quadrantL = assignQuadrant(lhs - origin);
            int quadrantR = assignQuadrant(rhs - origin);
            if (quadrantL != quadrantR) { return quadrantL < quadrantR; }
            
            return (Triangle(origin, lhs, rhs).signedArea() > 0);
        }
    };
    
    void Point2::grahamScan(std::vector<Point2>& hull, const std::vector<Point2>& points)
    {
        const int numPoints = (int) points.size();
        if (numPoints <= 3) { hull = points;  return; }
        
        const auto box = getExtremePoints(points);
        if (box.indexMinY == box.indexMaxY)
        {
            hull.resize((box.indexMinX == box.indexMaxX) ? 1 : 2);
            hull[0] = box.minX;
            if (box.indexMinX != box.indexMaxX) { hull[1] = box.maxX; }
            return;
        }
        
        hull = points;
        std::sort(hull.begin(), hull.end(), AngleCompare(box.minY));
        hull.push_back(hull[0]);
        
        int hullCount = 1; // Initialize stack.
        
        for (int i = 2; i <= numPoints; i++)
        {
            while (Triangle(hull[hullCount - 1], hull[hullCount], hull[i]).signedArea() <= 0)
            {
                if (hullCount > 1) { hullCount--;  continue; }
                if (i == numPoints) { break; } // Stack is empty.
                i++; // Keep searching.
            }
            // Otherwise point is on the boundary of the convex hull.
            hullCount++;
            std::swap(hull[hullCount], hull[i]);
        }
        
        hull.resize(hullCount);
    }
    
    /** Find points from pts that lie on the north side of segment (aa, bb)
     *  and lie in the convex hull.
     */
    static void subHull(const std::vector<Point2>& pts, const Point2& aa, const Point2& bb, std::vector<Segment2>& hullEdges)
    {
        int ptsSize = (int) pts.size();
        if (ptsSize == 0) { Segment2::add(hullEdges, aa, bb);  return; }
        Num maxSqDist = 0;
        Segment2 seg(aa,bb);
        int i = -1;
        int bestIndex = 0;
        Point2 farPoint;
        for (const auto& pt : pts)
        {
            ++i;
            Point2 temp;
            Num sqDist = seg.sqDistToLine(pt, temp);
            if ((i == 0) || (sqDist > maxSqDist))
            {
                bestIndex = i;
                farPoint = pt;
                maxSqDist = sqDist;
            }
        }
        Segment2 line1(aa, farPoint);
        Segment2 line2(farPoint, bb);
        Triangle tri(aa, farPoint, bb);
        std::vector<Point2> north1;
        std::vector<Point2> north2;
        i = -1;
        for (const auto& pt : pts)
        {
            ++i;
            if ((i == bestIndex))
            {
                north1.push_back(pt);
                north2.push_back(pt);
                continue;
            }
            if (tri.intersects(pt) != Triangle::Intersection::Exterior)
            { continue; }
            using Inter = Segment2::PointIntersection;
            const Inter side1 = line1.ptIntersects(pt);
            const Inter side2 = line2.ptIntersects(pt);
            if (side1 == Inter::North) { north1.push_back(pt); }
            if (side2 == Inter::North) { north2.push_back(pt); }
        }
        
        if (north1.size() == 1)
        {
            Segment2::add(hullEdges, aa, farPoint);
        }
        else { subHull(north1, aa, farPoint, hullEdges); }
        
        if (north2.size() == 1)
        {
            Segment2::add(hullEdges, farPoint, bb);
        }
        else { subHull(north2, farPoint, bb, hullEdges); }
    }
    
    void Point2::quickHull(std::vector<Segment2>& hullEdges, const std::vector<Point2>& points, const HullType ht)
    {
        hullEdges.resize(0);
        int ptsSize = (int) points.size();
        if (ptsSize <= 3)
        {
            if (ptsSize == 3) { Segment2::add(hullEdges, points[0], points[1], points[2]); }
            else if (ptsSize == 2) { Segment2::add(hullEdges, points[0], points[1]); }
            else if (ptsSize == 1) { Segment2::add(hullEdges, points[0]); }
            return;
        }
        Segment2 midline;
        const auto box = getExtremePoints(points);
        if (box.indexMinX == box.indexMaxX)
        {
            Segment2::add(hullEdges, box.minY, box.maxY);
            return;
        }
        midline = Segment2(box.minX, box.maxX);
            
        int i = -1;
        std::vector<Point2> north;
        std::vector<Point2> south;
        for (const auto& pt : points)
        {
            ++i;
            if ((i == box.indexMinX) || (i == box.indexMaxX)) { continue; }
            using Inter = Segment2::PointIntersection;
            const Inter side = midline.ptIntersects(pt);
            if      (side == Inter::North) { north.push_back(pt); }
            else if (side == Inter::South) { south.push_back(pt); }
        }
        if (ht != HullType::Lower) { subHull(north, box.minX, box.maxX, hullEdges); }
        if (ht != HullType::Upper) { subHull(south, box.maxX, box.minX, hullEdges); }
    }
    
    void Point2::delaunayTriangulation(std::vector<Segment2>& triEdges, const std::vector<Point2>& points)
    {
        std::vector<Plane3> hullFacets;
        {
            std::vector<Point3> points3d;
            for (const auto& pt : points)
            {
                points3d.push_back(Point3(pt.x, pt.y, pt.squareMag() * 0.5));
            }
            Point3::quickHull(hullFacets, points3d, HullType::Lower);
        }
        triEdges.resize(0);
        for (const auto& tri : hullFacets)
        {
            Point2 aa = Point2(tri.a.x, tri.a.y);
            Point2 bb = Point2(tri.b.x, tri.b.y);
            Point2 cc = Point2(tri.c.x, tri.c.y);
            triEdges.push_back(Segment2(aa, bb));
            triEdges.push_back(Segment2(bb, cc));
            triEdges.push_back(Segment2(cc, aa));
        }
    }
    
    Point2 Point2::moveToUnitRightTriangle(const Point2& a, const Point2& b, const Point2& c, const Point2& pt)
    {
        Num AA = b.y - a.y;
        Num BB = a.x - b.x;
        Num CC = c.y - a.y;
        Num DD = a.x - c.x;
        Num negDet = BB * CC - AA * DD;
        Num denominator = 1.0;
        bool divZero = false;
        try
        {
            denominator = 1.0 / negDet;
        }
        catch (...)
        {
            std::cout << "\nDivision by zero.";
            divZero = true;
        }
        if (divZero) { return pt; }
        Point2 pt_;
        Num u = pt.x - a.x;
        Num v = pt.y - a.y;
        pt_.x = (AA * u + BB * v) * denominator;
        pt_.y = (CC * u + DD * v) * denominator;
        return pt_;
    }
    
    Point2 Point2::operator+(const Point2& rhs) const
    {
        Point2 sum = *this;
        sum.x = sum.x + rhs.x;
        sum.y = sum.y + rhs.y;
        return sum;
    }
    
    Point2 Point2::operator-(const Point2& rhs) const
    {
        Point2 diff = *this;
        diff.x = diff.x - rhs.x;
        diff.y = diff.y - rhs.y;
        return diff;
    }
    
    Point2 Point2::operator*(const Num& scal) const
    {
        Point2 scaled = *this;
        scaled.x = scaled.x * scal;
        scaled.y = scaled.y * scal;
        return scaled;
    }
    
    Num Point2::dot(const Point2& rhs) const
    {
        return x * rhs.x + y * rhs.y;
    }
    
    Num Point2::squareMag() const
    {
        return dot(*this);
    }
    
    Num Point2::squareDistance(const Point2& rhs) const
    {
        Num dist_2 = 0;
        Num dt = x - rhs.x;
        dist_2 = dist_2 + dt * dt;
        dt = y - rhs.y;
        dist_2 = dist_2 + dt * dt;
        return dist_2;
    }
    
    BoundingBox Point2::getExtremePoints(const std::vector<Point2>& pts)
    {
        BoundingBox box;
        int i  = -1;
        //std::cout << "\n\nPts: ";
        for (const auto& pt : pts)
        {
            ++i;
            //std::cout << pt << ", ";
            if (i == 0) { box = BoundingBox(i, pt); continue; }
            if (pt.x < box.minX.x) { box.indexMinX = i;  box.minX = pt; }
            else if (pt.x > box.maxX.x) { box.indexMaxX = i;  box.maxX = pt; }
            if (pt.y < box.minY.y) { box.indexMinY = i;  box.minY = pt; }
            else if (pt.y > box.maxY.y) { box.indexMaxY = i;  box.maxY = pt; }
        }
        //std::cout << "\nBox: " << box << "\n\n";
        return box;
    }
}
