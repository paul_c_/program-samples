/*  Copyright Paul Cernea, June 20, 2019.
All Rights Reserved.*/

#ifndef BOUNDING_BOX_H
#define BOUNDING_BOX_H

#include "Point3.h"

namespace ComputationalGeometry
{
    struct BoundingBox
    {
        BoundingBox() : indexMinX(-1), indexMaxX(-1), indexMinY(-1), indexMaxY(-1), indexMinZ(-1), indexMaxZ(-1) {}
        BoundingBox(const int ind, const Point3& pt) : indexMinX(ind), indexMaxX(ind), indexMinY(ind), indexMaxY(ind), indexMinZ(-1), indexMaxZ(-1), minX(pt), maxX(pt), minY(pt), maxY(pt), minZ(pt), maxZ(pt)
        {
            if (pt.get_dimension() > 2)
            {
                indexMinZ = ind;
                indexMaxZ = ind;
            }
        }
        friend std::ostream& operator<<(std::ostream& str, const BoundingBox& box)
        {
            return str << "[ minX: " << box.indexMinX << ", " << box.minX <<
            ", maxX: "  << box.indexMaxX << ", " << box.maxX <<
            ", minY: "  << box.indexMinY << ", " << box.minY <<
            ", maxY: "  << box.indexMaxY << ", " << box.maxY <<
            ", minZ: "  << box.indexMinZ << ", " << box.minZ <<
            ", maxZ: "  << box.indexMaxZ << ", " << box.maxZ << "]";
        }
        int indexMinX;
        int indexMaxX;
        int indexMinY;
        int indexMaxY;
        int indexMinZ;
        int indexMaxZ;
        Point3 minX;
        Point3 maxX;
        Point3 minY;
        Point3 maxY;
        Point3 minZ;
        Point3 maxZ;
    };
}

#endif //def BOUNDING_BOX_H
