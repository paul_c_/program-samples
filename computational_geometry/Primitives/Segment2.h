/*  Copyright Paul Cernea, May 26, 2019.
All Rights Reserved.*/

#ifndef SEGMENT_2_H
#define SEGMENT_2_H

#include "Point2.h"

namespace ComputationalGeometry
{
    struct Segment2
    {
        Segment2() = default;
        Segment2(const Point2& pp, const Point2& qq) : p(pp), q(qq) {}
        Point2 p;
        Point2 q;
        enum Intersection { Exterior = 0, Vertex = 1, Interior = 2 };
        Intersection intersects(const Segment2& rhs, double& param, double& rhsParam) const;
        enum PointIntersection { South = 0, North = 1, CollinearExterior = 2, VertexP = 3, VertexQ = 4, CollinearInterior = 5 };
        PointIntersection ptIntersects(const Point2& pt) const;
        Num sqDistToLine(const Point2& pt, Point2& best) const;
        static void add(std::vector<Segment2>& pts, const Point2& pt0);
        static void add(std::vector<Segment2>& pts, const Point2& pt0, const Point2& pt1);
        static void add(std::vector<Segment2>& pts, const Point2& pt0, const Point2& pt1, const Point2& pt2);
    };
}

#endif // SEGMENT_2_H
