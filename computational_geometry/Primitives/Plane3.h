/*  Copyright Paul Cernea, May 26, 2019.
All Rights Reserved.*/

#ifndef PLANE_3_H
#define PLANE_3_H

#include "Point3.h"

namespace ComputationalGeometry
{
    struct Plane3
    {
        Plane3() = default;
        Plane3(const Point3& aa, const Point3& bb, const Point3& cc) : a(aa), b(bb), c(cc) {}
        Point3 a;
        Point3 b;
        Point3 c;
        enum Intersection { South = 0, North = 1, Interior = 2 };
        Intersection intersects(const Point3& pt);
        Num sqDistToPlane(const Point3& pt, Point3& best) const;
    };
}

#endif // PLANE_3_H
