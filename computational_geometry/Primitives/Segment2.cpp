/*  Copyright Paul Cernea, May 25, 2019.
All Rights Reserved.*/

#include "Segment2.h"

namespace ComputationalGeometry
{
    Segment2::Intersection Segment2::intersects(const Segment2& rhs, double& param, double& rhsParam) const
    {
        double& t = param;
        double& s = rhsParam;
        t = s = -1;

        double u = p.x - rhs.p.x;
        double v = p.y - rhs.p.y;
        const double tol = 1.0e-9;
        if (Point3::absVal(u) <= tol && Point3::absVal(v) <= tol)
        {
            t = s = 0;  return Vertex;
        }
        if (Point3::absVal(p.x - rhs.q.x) <= tol && Point3::absVal(p.y - rhs.q.y) <= tol)
        {
            t = 0;  s = 1;  return Vertex;
        }
        if (Point3::absVal(q.x - rhs.p.x) <= tol && Point3::absVal(q.y - rhs.p.y) <= tol)
        {
            t = 1;  s = 0;  return Vertex;
        }
        if (Point3::absVal(q.x - rhs.q.x) <= tol && Point3::absVal(q.y - rhs.q.y) <= tol)
        {
            t = 1;  s = 1;  return Vertex;
        }
        double AA = rhs.q.x - rhs.p.x;
        double BB = p.x - q.x;
        double CC = rhs.q.y - rhs.p.y;
        double DD = p.x - q.x;
        double det = AA * DD - CC * BB;
        if (Point3::absVal(det) <= tol)
        {
            if (ptIntersects(rhs.p) == CollinearInterior) { return Interior; }
            if (ptIntersects(rhs.q) == CollinearInterior) { return Interior; }
            return Exterior;
        }
        double detInv = 1.0 / det;
        t = detInv * (DD * u - BB * v);
        s = detInv * (AA * v - CC * u);
        if ((t < 0) || (t > 1)) { return Exterior; }
        if ((s < 0) || (s > 1)) { return Exterior; }
        return Interior;
    }
    
    Segment2::PointIntersection Segment2::ptIntersects(const Point2& pt) const
    {
        Num ptx = pt.x - p.x;
        Num pty = pt.y - p.y;
        Num AA = q.x - p.x;
        Num BB = q.y - p.y;
        Num CC = -BB;
        Num DD = AA;
        Num det = AA * DD - BB * CC;
        const double tol = 1.0e-6;
        if (Point3::absVal(det) <= tol)
            return VertexP;
        Num oneOverDet = 1.0 / det;
        Num ux = (AA * ptx + BB * pty) * oneOverDet;
        Num uy = (CC * ptx + DD * pty) * oneOverDet;
        if (Point3::absVal(uy) <= tol)
        {
            if (Point3::absVal(ux) <= tol)
                return VertexP;
            if (Point3::absVal(ux - 1) <= tol)
                return VertexQ;
            if ((ux < 0) || (ux > 1))
                return CollinearExterior;
            return CollinearInterior;
        }
        if (uy > 0) { return North; }
        return South;
    }
    
    Num Segment2::sqDistToLine(const Point2& pt, Point2& best) const
    {
        Point2 qq = q - p;
        Point2 pt_ = pt - p;
        Num segLength2 = qq.squareMag();
        Num proj = pt_.squareMag();
        if (segLength2 < 1.0e-6) { best = p;  return proj; }
        best = p + qq * (proj / segLength2);
        return best.squareDistance(pt);
    }
    
    void Segment2::add(std::vector<Segment2>& pts, const Point2& pt0)
    {
        pts.push_back(Segment2(pt0, pt0));
    }
    
    void Segment2::add(std::vector<Segment2>& pts, const Point2& pt0, const Point2& pt1)
    {
        pts.push_back(Segment2(pt0, pt1));
    }
    
    void Segment2::add(std::vector<Segment2>& pts, const Point2& pt0, const Point2& pt1, const Point2& pt2)
    {
        pts.push_back(Segment2(pt0, pt1));
        pts.push_back(Segment2(pt1, pt2));
        pts.push_back(Segment2(pt2, pt0));
    }
}
