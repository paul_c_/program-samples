/*  Copyright Paul Cernea, May 30, 2017.
All Rights Reserved.*/

#ifndef POINT_3_H
#define POINT_3_H

#include "../Includes/includes.h"

typedef double Num;

namespace ComputationalGeometry
{
    class Plane3;
    struct BoundingBox;
    enum HullType { Full = 0, Upper = 1, Lower = 2 };
    
	class Point3
	{
	public:
        static const Num PI = 3.14159265;
		Num x;  Num y;  Num z;
		Point3() : x(0), y(0), z(0) {}
		Point3(const Num& xx, const Num& yy, const Num& zz) : x(xx), y(yy), z(zz) {}
		
		virtual int get_dimension() const { return 3; }
        static Num absVal(const Num& val);
		
		/** Necessary for set insertion to work.
		 */
        bool operator<(const Point3& q) const;
        bool operator==(const Point3& q) const;
        void print(const std::string& prequel="") const;
        static Num sq_distance(const Point3& P, const Point3& Q);
		
		template <class Container> static Num naive_min_sq_distance(Container& A, Container& B, Point3& A_min, Point3& B_min)
		{
			Num min = 0;  bool started = false;
			for (typename Container::iterator it1 = A.begin(); it1 != A.end(); ++it1)
			{
				for (typename Container::iterator it2 = B.begin(); it2 != B.end(); ++it2)
				{
					// Note: set iteration takes place in sorted order.
					//std::cout << "[";  it2->print();  std::cout << "]\n";
					if (!started)
					{
						min = Point3::sq_distance(*it1, *it2);
						A_min = *it1;
						B_min = *it2;
						started = true;
						continue;
					}
					Num candidate = Point3::sq_distance(*it1, *it2);
					if (candidate >= min) {continue;}
				
					min = candidate;
					A_min = *it1;
					B_min = *it2;
					if (min == 0) {break;}
				}
				if (min == 0) {break;}
			}
			return min;
		}
		
		template <class Container> static Num naive_min_sq_distance(Container& arr, Point3& min_1, Point3& min_2)
		{
			Num min = 0;  bool started = false;
			if (arr.begin() != arr.end())
			{
				min_1 = *(arr.begin());
				min_2 = *(arr.begin());
			}
			for (typename Container::iterator it1 = arr.begin(); it1 != arr.end(); ++it1)
			{
				for (typename Container::iterator it2 = arr.begin(); it2 != arr.end(); ++it2)
				{
					if (it1 == it2) {continue;}
					// Note: set iteration takes place in sorted order.
					//std::cout << "[";  it2->print();  std::cout << "]\n";
					if (!started)
					{
						min = Point3::sq_distance(*it1, *it2);
						min_1 = *it1;
						min_2 = *it2;
						started = true;
						continue;
					}
					Num candidate = Point3::sq_distance(*it1, *it2);
					if (candidate >= min) {continue;}
				
					min = candidate;
					min_1 = *it1;
					min_2 = *it2;
					if (min == 0) {break;}
				}
				if (min == 0) {break;}
			}
			return min;
		}
        
        Point3 cross(const Point3& rhs) const;
        Point3 operator+(const Point3& rhs) const;
        Point3 operator-(const Point3& rhs) const;
        Num dot(const Point3& rhs) const
        {
            return x * rhs.x + y * rhs.y + z * rhs.z;
        }
        Num squareMag() const
        {
            return dot(*this);
        }
        Num squareDistance(const Point3& rhs) const
        {
            Num dist_2 = 0;
            Num dt = x - rhs.x;
            dist_2 = dist_2 + dt * dt;
            dt = y - rhs.y;
            dist_2 = dist_2 + dt * dt;
            dt = z - rhs.z;
            dist_2 = dist_2 + dt * dt;
            return dist_2;
        }
        Point3 operator*(const Num& scal) const
        {
            Point3 scaled = *this;
            scaled.x = scaled.x * scal;
            scaled.y = scaled.y * scal;
            scaled.z = scaled.z * scal;
            return scaled;
        }
        friend std::ostream& operator<<(std::ostream& str, const Point3& pt);
        
        /** Quick hull runs worst case in O(n^2) time where n is the number of points.
         */
        static void quickHull(std::vector<Plane3>& hullFacets, const std::vector<Point3>& points, const HullType ht = HullType::Full);
        
        static BoundingBox getExtremePoints(const std::vector<Point3>& pts);
	};
}

#endif //def POINT_3_H
