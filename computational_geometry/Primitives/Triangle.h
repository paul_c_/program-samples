/*  Copyright Paul Cernea, May 26, 2019.
All Rights Reserved.*/

#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Segment2.h"

namespace ComputationalGeometry
{
    struct Triangle
    {
        Triangle() = default;
        Triangle(const Point2& aa, const Point2& bb, const Point2& cc) : a(aa), b(bb), c(cc) {}
        Point2 a;
        Point2 b;
        Point2 c;
        enum Intersection { Exterior = 0, Boundary = 1, Interior = 2, VertexA = 3, VertexB = 4, VertexC = 5 };
        Intersection intersects(const Point2& pt) const;
        static void basicTriangulate(std::vector<Segment2>& edges, const std::vector<Point2>& points, const unsigned& N);
        Num signedArea() const;
        Num area() const;
    };
}

#endif // TRIANGLE_H
