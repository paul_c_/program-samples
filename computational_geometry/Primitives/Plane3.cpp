/*  Copyright Paul Cernea, May 26, 2019.
All Rights Reserved.*/

#include "../Mathematics/Matrix3.h"
#include "Plane3.h"

namespace ComputationalGeometry
{
    Plane3::Intersection Plane3::intersects(const Point3& pt)
    {
        Point3 bb = b - a;
        Point3 cc = c - a;
        Point3 bXc = bb.cross(cc);
        Matrix3 transform = Matrix3(bb, cc, bXc);
        transform = transform.transpose();
        transform = transform.inv();
        Point3 p = transform.on(pt - a);
        const Num tol = 1.0e-6;
        if (Point3::absVal(p.z) <= tol) { return Interior; }
        if (p.z > 0) { return North; }
        return South;
    }
    
    Num Plane3::sqDistToPlane(const Point3& pt, Point3& best) const
    {
        Point3 pp = pt - a;
        Point3 bb = b - a;
        Point3 cc = c - a;
        Num b2 = bb.squareMag();
        Num c2 = cc.squareMag();
        Num pDotB = pp.dot(bb);
        Num pDotC = pp.dot(cc);
        Num bDotC = bb.dot(cc);
        Num denom = b2 * c2 - bDotC * bDotC;
        if (Point3::absVal(denom) < 1.0e-6)
        {
            // TODO: Fill this in.
            best = pt;  return 0;
        }
        Num oneOverDenom = 1.0 / denom;
        Num t = (pDotB * c2 - bDotC * pDotC) * oneOverDenom;
        Num s = (pDotC * b2 - bDotC * pDotB) * oneOverDenom;
        best = a + bb * t + cc * s;
        return best.squareDistance(pt);
    }
}
