/*  Copyright Paul Cernea, May 25, 2019.
All Rights Reserved.*/

#include "../Primitives/Point2.h"

void basicTest()
{
    using namespace ComputationalGeometry;
    {
        Point3 P(1.0, 2.0, 3.0);
        
        std::cout << "\n//////\nThe dimension is " << P.get_dimension() << ".";
        
        P.print("\n");
        std::cout << "\n";
        
        Point2 Q(1.0, 2.0);
        
        std::cout << "\n//////\nThe dimension is " << Q.get_dimension() << ".";
        
        Q.print("\n");
        std::cout << "\n";
    }
    
    {
        Point3 P(1.0, 2.0, 3.0);
        Point3 Q(1.0, -2.0, 3.0);
        
        printf("%f\n", Point3::sq_distance(P,Q));
    }
    
    {
        Point2 P(1.5, 2.0);
        Point2 Q(1.0, -2.0);
        
        printf("%f\n", Point2::sq_distance(P,Q));
    }
    
    {
        std::set<Point3 > A;
        Point3 a(1.5, 2.0, 0);
        Point3 b(1.0, 3.0, 0);
        Point3 c(-1.5, 7.0, 0);
        
        std::set<Point3 > B;
        Point3 d(4.5, 2.3, 0);
        Point3 e(-1.55, 2.6, 0);
        Point3 f(88.3, 0.001, 0);
        
        A.insert(a);
        A.insert(b);
        A.insert(c);
        
        B.insert(d);
        B.insert(e);
        B.insert(f);
        
        Point3 p, q;
        
        double min = Point3::naive_min_sq_distance(A, B, p, q);
        
        std::cout << "\n//////\n" << min;
        p.print("\n");
        q.print("\n");
        std::cout << "\n";
    }
    
    {
        std::set<Point2 > A;
        Point2 a(1.5, 2.0);
        Point2 b(1.0, 3.0);
        Point2 c(-1.5, 7.0);
        
        std::set<Point2 > B;
        Point2 d(4.5, 2.3);
        Point2 e(-1.35, 2.6);
        Point2 f(88.3, 0.001);
        
        A.insert(a);
        A.insert(b);
        A.insert(c);
        
        B.insert(d);
        B.insert(e);
        B.insert(f);
        
        Point2 p, q;
        
        double min = Point2::naive_min_sq_distance(A, B, p, q);
        
        std::cout << "\n//////\n" << min << "\n";
        p.print("\n");
        q.print("\n");
        std::cout << "\n";
    }
    
    {
        std::set<Point2 > A;
        Point2 a(1.5, 2.0);
        Point2 b(1.0, 3.0);
        Point2 c(-1.5, 7.0);
        Point2 d(4.5, 2.3);
        Point2 e(-1.35, 2.6);
        Point2 f(88.3, 0.001);
        
        A.insert(a);
        A.insert(b);
        A.insert(c);
        A.insert(d);
        A.insert(e);
        A.insert(f);
        
        Point2 p, q;
        
        double min = Point2::naive_min_sq_distance(A, p, q);
        
        std::cout << "\n//////\n";
        std::cout << min << "\n";
        p.print();  std::cout << "\n";
        q.print();  std::cout << "\n";
        
        min = Point2::min_sq_distance(A, p, q);
        
        std::cout << "\n/!!!!/\n";
        std::cout << min << "\n";
        p.print();  std::cout << "\n";
        q.print();  std::cout << "\n";
    }
}

