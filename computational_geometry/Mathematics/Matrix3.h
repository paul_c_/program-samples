/*  Copyright Paul Cernea, May 26, 2019.
All Rights Reserved.*/

#ifndef MATRIX_3_H
#define MATRIX_3_H

#include "../Primitives/Point3.h"

namespace ComputationalGeometry
{
    struct Matrix3
    {
        Matrix3() = default;
        Matrix3(const Point3& aa, const Point3& bb, const Point3& cc) : a(aa), b(bb), c(cc) {}
        Point3 a;
        Point3 b;
        Point3 c;
        Num determinant() const;
        Matrix3 transpose() const;
        Matrix3 inv() const;
        Point3 on(const Point3& pt) const;
    };
}

#endif // MATRIX_3_H
