/*  Copyright Paul Cernea, May 26, 2019.
All Rights Reserved.*/

#include "Matrix3.h"

namespace ComputationalGeometry
{
    Num Matrix3::determinant() const
    {
        Num det = 0;
        det = det + a.x * (b.y * c.z - c.y * b.z);
        det = det + a.y * (b.z * c.x - c.z * b.x);
        det = det + a.z * (b.x * c.y - c.x * b.y);
        return det;
    }
    
    Matrix3 Matrix3::transpose() const
    {
        Matrix3 i;
        i.a.x = a.x;
        i.b.x = a.y;
        i.c.x = a.z;
        i.a.y = b.x;
        i.b.y = b.y;
        i.c.y = b.z;
        i.a.z = c.x;
        i.b.z = c.y;
        i.c.z = c.z;
        return i;
    }
    
    Matrix3 Matrix3::inv() const
    {
        Matrix3 i;
        Num oneOverDet = 0;
        bool invertible = true;
        try
        {
            oneOverDet = determinant();
        }
        catch (...)
        {
            std::cout << "\nMatrix not invertible.  Determinant is zero.";
            invertible = false;
        }
        if (!invertible)
            return i;
        i.a.x = (b.y * c.z - b.z * c.y) * oneOverDet;
        i.b.x = (b.z * c.x - b.x * c.z) * oneOverDet;
        i.c.x = (b.x * c.y - b.y * c.x) * oneOverDet;
        i.a.y = (c.y * a.z - c.z * a.y) * oneOverDet;
        i.b.y = (c.z * a.x - c.x * a.z) * oneOverDet;
        i.c.y = (c.x * a.y - c.y * a.x) * oneOverDet;
        i.a.z = (a.y * b.z - a.z * b.y) * oneOverDet;
        i.b.z = (a.z * b.x - a.x * b.z) * oneOverDet;
        i.c.z = (a.x * b.y - a.y * b.x) * oneOverDet;
        return i;
    }
    
    Point3 Matrix3::on(const Point3& pt) const
    {
        Point3 pt_;
        pt_.x = a.x * pt.x + a.y * pt.y + a.z * pt.z;
        pt_.y = b.x * pt.x + b.y * pt.y + b.z * pt.z;
        pt_.z = c.x * pt.x + c.y * pt.y + c.z * pt.z;
        return pt_;
    }
}
