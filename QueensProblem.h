//
//  QueensProblem.h
//
//  Created by Paul Cernea on 2/14/15.
//  Copyright (c) 2015 Paul Cernea. All rights reserved.
//

#ifndef QUEENS_PROBLEM_H
#define QUEENS_PROBLEM_H
#include <vector>
#include <iostream>

/*! \file QueensProblem.h
 \brief Prints all mutually non-attacking queen (or rook) configurations.
 */

namespace programSamples
{
    /**
     * \brief Encapsulates the size of a chessboard, and provides methods to print
     * mutually non-attacking queen (or rook) configurations.
     */
    class Chessboard
    {
        /** The size of the chessboard.
         */
        const int N;
        /** \brief Generates all possible configurations of mutually non-attacking rooks.
         *
         * @param NN The size of the chessboard.
         *
         * The mutually non-attacking rooks on a chessboard are
         * choices of elements in a square matrix whose rows and columns are
         * mutually noninterfering.  They are determined recursively.
         *
         * @return A square array of booleans representing whether a rook occupies
         * that respective square.
         */
        static std::vector<std::vector<std::vector<bool> > > get_rook_configurations(const int & NN)
        {
            std::vector<std::vector<std::vector<bool> > > answer;
            if (NN <= 0) {return answer;}
            if (NN == 1)
            {
                std::vector<bool> column(1);  column[0] = true;
                std::vector<std::vector<bool> > row(1);
                row[0] = column;
                answer.resize(1);  answer[0] = row;
                return answer;
            }
            std::vector<std::vector<std::vector<bool> > > smaller_answer = get_rook_configurations(NN - 1);
        
            for (int i = 0; i < NN; i++)
            {
                int count = 0;
                int siz = (int) smaller_answer.size();
                for (; count < siz; count++)
                {
                    std::vector<std::vector<bool> > current(NN);
                    for (int II = 0; II < NN; II++)
                    {
                        current[II].resize(NN);
                    }
                    for (int II = 0; II < NN; II++)
                    {
                        int II_offset = 0;
                        if (II >= i) {II_offset = 1;}
                        for (int JJ = 0; JJ < NN; JJ++)
                        {
                            current[II][JJ] = false;
                            if (II == i || JJ == 0) {continue;}
                            current[II][JJ]
                            = smaller_answer[count][II - II_offset][JJ - 1];
                        }
                    }
                    current[i][0] = true;
                    answer.push_back(current);
                }
            }
            return answer;
        }
public:
        /**
         * @param NN The size of the chessboard.
         */
        Chessboard(const unsigned & NN) : N(NN) {}
        
        ~Chessboard() {}
        /** \brief Prints all configurations of N mutually non-attacking rooks, where
         *  N is the size of the chessboard.
         */
        void print_rook_configurations() const
        {
            std::vector<std::vector<std::vector<bool> > > configs = Chessboard::get_rook_configurations(N);
            int num_configs = (int) configs.size();
            for (int count = 0; count < num_configs; count++)
            {
                std::cout << "Rook configuration " << (count + 1) << ":\n";
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        if (configs[count][i][j])
                        {
                            std::cout << "[R]";
                        }
                        else {std::cout << "[ ]";}
                    }
                    std::cout << std::endl;
                }
            }
        }
        /** \brief Prints all configurations of N mutually non-attacking queens,
         * where N is the size of the chessboard.
         */
        void print_queen_configurations() const
        {
            std::vector<std::vector<std::vector<bool> > > configs = Chessboard::get_rook_configurations(N);
            int num_configs = (int) configs.size();
            int queen_count = 0;
            for (int count = 0; count < num_configs; count++)
            {
                bool okay = true;
                std::vector<std::vector<bool> > current = configs[count];
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        if (current[i][j] == false) {continue;}
                        for (int k = -N; k <= N; k++)
                        {
                            if (k == 0) {continue;}
                            if ((i + k) < 0 || (i + k) >= N) {continue;}
                            if ((j + k) < 0 || (j + k) >= N) {continue;}
                            if (current[i+k][j+k]) {okay = false;}
                            if (!okay) {break;}
                        }
                        if (!okay) {break;}
                        for (int k = -N; k <= N; k++)
                        {
                            if (k == 0) {continue;}
                            if ((i + k) < 0 || (i + k) >= N) {continue;}
                            if ((j - k) < 0 || (j - k) >= N) {continue;}
                            if (current[i+k][j-k]) {okay = false;}
                            if (!okay) {break;}
                        }
                        if (!okay) {break;}
                    }
                    if (!okay) {break;}
                }
                if (!okay) {continue;}
                queen_count++;
                std::cout << "Queen configuration " << queen_count << ":\n";
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        if (configs[count][i][j])
                        {
                            std::cout << "[Q]";
                        }
                        else {std::cout << "[ ]";}
                    }
                    std::cout << std::endl;
                }
            }
        }
    };

    /** \brief Runs some tests demonstrating the soundness of the configuration-printing methods.
     *
     *  Prints the mutually non-attacking rook configurations on a 3 x 3 chessboard.
     *  Note that there would be none if we chose queen configurations on such a
     *  chessboard, as one can verify by visualizing these rooks as queens.
     *
     *  Then prints the mutually non-attacking queen configurations on an 8 x 8
     *  chessboard.  There are 92 such configurations.
     */
    void testChessboard()
    {
        {
            Chessboard chess(3);
            chess.print_rook_configurations();
        }
        {
            Chessboard chess(8);
            chess.print_queen_configurations();
        }
    }
}

#endif
