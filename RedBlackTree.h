//
//  RedBlackTree.h
//  AlgorithmsStructures
//
//  Created by Paul Cernea on 6/2/15.
//  Copyright (c) 2015 Paul Cernea. All rights reserved.
//

#ifndef AlgorithmsStructures_RedBlackTree_h
#define AlgorithmsStructures_RedBlackTree_h

#include <iostream>
#include <string>

/*! \file RedBlackTree.h
 \brief Implements the Red-Black Tree data structure.
 */

namespace programSamples
{
    const bool RED   = true;  /**< A placeholder for the color red.*/
    const bool BLACK = false; /**< A placeholder for the color black.*/
    
    template <class T> class RedBlackTree;
    
    /** \brief Encapsulates the information contained in a node of
     * a red-black tree.
     *
     * @param U A template parameter representing the type of data stored
     * by a node of a red-black tree.
     */
    template<class U> struct RBT_Node
    {
        bool color;  /**< Auxiliary data.  This can either be red or black.*/
        /** \brief Determines if the node is black or red.
         *
         * @param nd The node whose color is to be determined.
         * If a node is <code>NULL</code>, this method returns black,
         * since by convention
         * the null leaves of a red-black tree are black.
         *
         * @return <code>true</code> if a node is black, <code>false</code>
         * if it is red.
         */
        static bool is_black(const RBT_Node<U> * nd)
        {
            if (nd == NULL) {return true;}
            return (nd->color == BLACK);
        }
        /** The node's left child.  Its value must be <code>NULL</code> if the
         * left child does not exist.
         */
        RBT_Node * left;
        /** The node's right child.  Its value must be <code>NULL</code> if the
         * right child does not exist.
         */
        RBT_Node * right;
        /**The node's parent.  Its value must be <code>NULL</code> if the
         * parent does not exist.  That will only happen when the node is the
         * root of its ambient tree.
         */
        RBT_Node * parent;
        U * object_ptr;  /**< A pointer to the node's satellite data.*/
        
        /**
         * Frees the object pointer's satellite data if the object pointer points to
         * valid memory.
         */
        ~RBT_Node() {if (object_ptr) {delete object_ptr;}}
        /** \brief Initialization with satellite data.
         *
         * @param item The item to which the satellite
         * data object pointer will refer.
         *
         * By default, the color is set to black, and
         * the left-child, right-child, and parent
         * pointers are set to <code>NULL</code>.
         * The object pointer allocates memory to hold a copy of the item, and
         * the item is copied into that slot.  That memory is freed later by
         * the destructor.
         */
        RBT_Node(const U& item)
        {
            color      = BLACK;
            left       = NULL;
            right      = NULL;
            parent     = NULL;
            object_ptr = new U(item);
        }
        /** \brief Prints a node's satellite data, as well as its links.
         *
         * Besides printing a node's satellite data, its children and parents
         * are also printed, along with its red or black color.
         */
        void print() const
        {
            if (color == BLACK) {std::cout << "b ";}
            else /*..........*/ {std::cout << "r ";}
            std::cout << "(";
            if (left) {std::cout << *(left->object_ptr);}
            else {std::cout << "NULL";}
            std::cout << ")<-(";
            std::cout << (*object_ptr);
            std::cout << ")->(";
            if (right) {std::cout << *(right->object_ptr);}
            else {std::cout << "NULL";}
            if (parent)
            {
                std::cout << ")  Parent = (";
                std::cout << *(parent->object_ptr);
                std::cout << ")\n";
            }
            else {std::cout << ")  [ROOT]\n";}
        }
        /** \brief Determines whether a node has any children.
         *
         * @return <code>true</code> if a node has at least one child,
         * <code>false</code> otherwise.
         */
        bool has_children() const
        {
            if (left)  {return true;}
            if (right) {return true;}
            return false;
        }
        
        /** \brief Determines whether a node is the left child of its parent.
         *
         * @return <code>true</code> if the node is the left child of its parent,
         * <code>false</code> otherwise.  Note that <code>false</code> is returned
         * if the node has no parent.
         */
        bool is_left_child() const
        {
            if (parent == NULL) {return false;}
            return (this == parent->left);
        }
        
        /** \brief Determines whether a node is the right child of its parent.
         *
         * @return <code>true</code> if the node is the right child of its parent,
         * <code>false</code> otherwise.  Note that <code>false</code> is returned
         * if the node has no parent.
         */
        bool is_right_child() const
        {
            if (parent == NULL) {return false;}
            return (this == parent->right);
        }
        
        /** \brief Recursively deletes a node and its children.
         *
         * Recursively deletes a node and its children.  This routine is
         * used in the destructor for a tree containing these nodes.
         */
        void delete_with_children()
        {
            if (left)
            {
                if (left->has_children())
                {
                    left->delete_with_children();
                }
                delete left;
            }
            if (right)
            {
                if (right->has_children())
                {
                    right->delete_with_children();
                }
                delete right;
            }
            //Do NOT delete parent.
        }
        /** \brief Returns the next in-order node, if it exists.
         *
         * @return The next in-order node, if such a node exists.  Note that the
         * routine does not need to refer
         * to any tree the node might be contained in.
         */
        RBT_Node<U> * next()
        {
            if (right) {return RedBlackTree<U>::find_smallest(right);}
            RBT_Node<U> * answer = this;
            while (answer->parent)
            {
                if (answer->parent->right == answer)
                {
                    answer = answer->parent;
                    continue;
                }
                return answer->parent;
            }
            return NULL;
        }
    };
    
    
    /** \brief A binary search tree that balances itself to maintain height
     * O(log(n)).
     *
     * @param T A template parameter indicating the type of data stored in the tree.
     *
     * A red-black tree is a type of binary search tree, which means that it must
     * obey the following rules:
     * - Every node has precisely two child nodes.  Either child may be null.
     * - The information stored in a left child is
     * less than or equal to the information stored in its parent.
     * - The information stored in a right child is greater than the information
     * stored in its parent.
     *
     * Besides being a binary search tree, a red-black tree must satisfy the
     * following rules:
     * - Each node must be red or black, with the root and the
     * null leaves being black.
     * - If a node is red, both of its children must be
     * black.
     * - For _each_ node, all paths from that node to its null
     * leaves must have the same number of black nodes.  This is called the
     * _black height_ of the node.  The invariance of black height
     * ensures that the overall height is logarithmic in the size of the tree.
     */
    template <class T> class RedBlackTree
    {
        RBT_Node<T> * root; /**< The root of the tree.*/
        bool unique_keys; /**< A flag indicating whether keys are unique.*/
        
        /** \brief Finds a node with the given key, if one exists.
         *
         * @param key The key that must be found.
         *
         * This routine takes O(log(n)) time.
         *
         * @return The node with the given key, or <code>NULL</code> if none exists.
         */
        RBT_Node<T> * find_(const T& key) const
        {
            RBT_Node<T> * answer = root;
            while (answer != NULL)
            {
                if (key == *(answer->object_ptr)) {return answer;}
                if (key <  *(answer->object_ptr)) {answer = answer->left;  continue;}
                answer = answer->right;
            }
            return answer;
        }
        
        /** \brief Performs a left rotation on node B.
         *
         * A left rotation is an order-preserving isomorphism of
         * a binary search tree.  It uses constant space and time.  The procedure
         * works as follows:
         *
         * (parent)->(B) and ((a)) <--- (B) ---> (C) and
         * ((b)) <--- (C) ---> ((c))
         *
         * becomes
         *
         * (parent)->(C) and (B) <--- (C) ---> ((c)) and
         * ((a)) <--- (B) ---> ((b))
         *
         * Here ((a)), ((b)), and ((c)) are sub-trees themselves,
         * while (parent), (B), and (C) are nodes.
         * @param B is a red-black-tree node about which the rotation occurs.
         */
        void left_rotation(RBT_Node<T> * B)
        {
            if (B == NULL) {return;}
            RBT_Node<T> * C = B->right;
            
            if (C == NULL) {return;}
            RBT_Node<T> * b = C->left;
            
            B->right = b;
            if (b != NULL) {b->parent = B;}
            
            RBT_Node<T> * parent_B = B->parent;
            
            C->parent = parent_B;
            
            if (parent_B == NULL) {root = C;}
            else
            {
                if (B == parent_B->left) {C->parent->left  = C;}
                else /*...............*/ {C->parent->right = C;}
            }
            
            C->left   = B;
            B->parent = C;
        }
        
        /** \brief Performs a right rotation on node C.
         *
         * A right rotation is an order-preserving isomorphism of
         * a binary search tree.  It uses constant space and time.  The procedure
         * works as follows:
         *
         * (parent)->(C) and (B) <--- (C) ---> ((c)) and
         * ((a)) <--- (B) ---> ((b))
         *
         * becomes
         *
         * (parent)->(B) and ((a)) <--- (B) ---> (C) and
         * ((b)) <--- (B) ---> ((c))
         *
         * Here ((a)), ((b)), and ((c)) are trees themselves, while (parent), (B),
         * and (C) are nodes.
         * @param C is a red-black-tree node about which the rotation occurs.
         */
        void right_rotation(RBT_Node<T> * C)
        {
            if (C == NULL) {return;}
            RBT_Node<T> * B = C->left;
            
            if (B == NULL) {return;}
            RBT_Node<T> * b = B->right;
            
            C->left = b;
            if (b != NULL) {b->parent = C;}
            
            RBT_Node<T> * parent_C = C->parent;
            
            B->parent = parent_C;
            
            if (parent_C == NULL) {root = B;}
            else
            {
                if (C == parent_C->left) {B->parent->left  = B;}
                else /*...............*/ {B->parent->right = B;}
            }
            
            B->right  = C;
            C->parent = B;
        }
        
        /** \brief Finds the node with the largest key.
         *
         * This routine takes O(log(n)) time.
         *
         * @return The node with the largest key, or <code>NULL</code> if the
         * tree is empty.
         */
        RBT_Node<T> * find_largest(RBT_Node<T> * current_root) const
        {
            if (current_root == NULL) {return NULL;}
            RBT_Node<T> * answer = current_root;
            while (answer->right != NULL)
            {
                answer = answer->right;
            }
            return answer;
        }
        
        /**  \brief Finds the node with the smallest key.
         *
         * This routine takes O(log(n)) time.
         *
         * @return The node with the smallest key, or <code>NULL</code> if the tree
         * is empty.
         */
        RBT_Node<T> * in_order_begin() const
        {
            return find_smallest(root);
        }
        
        /**  \brief Returns <code>NULL</code>, modeling the behavior of standard
         * iterators.
         *
         *  @return <code>NULL</code>
         */
        RBT_Node<T> * in_order_end() const
        {
            return NULL;
        }
        
        /**  \brief Finds the next in-order node in the tree.
         *
         * @param current The current node.
         *
         * This routine takes O(log(n)) time.  This worst-case time cannot be
         * improved upon, or it would lead to a general sorting algorithm
         * faster than O(n * log(n)).
         *
         * @return A unique node depending on the properties of the tree.
         * This node may be
         * - <code>NULL</code>, if the key of the current node is the
         * largest in the tree.
         * - a node whose key equals that of the current node.
         * - a node with the smallest key greater than that of the current node.
         * The return node must satisfy one of these three properties.
         */
        RBT_Node<T> * in_order_next(RBT_Node<T> * current) const
        {
            if (current == NULL) {return NULL;}
            if (current->right) {return find_smallest(current->right);}
            RBT_Node<T> * answer = current;
            while (answer->parent)
            {
                if (answer->parent->right == answer)
                {
                    answer = answer->parent;
                    continue;
                }
                return answer->parent;
            }
            return NULL;
        }
        
        /**  \brief Removes a single node from the red-black tree.
         *
         * @param node The node to be removed.
         *
         * This routine takes O(log(n)) time.  The node must be a node in
         * the tree.
         */
        void remove_single_(RBT_Node<T> * node)
        {
            if (node == NULL) {return;}
            
            RBT_Node<T> * to_erase;
            if ((node->left == NULL) || (node->right == NULL))
            {
                to_erase = node;
            }
            else
            {
                // Find in-order successor (or in-order predecessor), as in
                // binary search tree deletion.  Copy its data into node, and
                // then delete it.  This cannot break RBT structure.
                to_erase = in_order_next(node);
                //to_erase cannot be NULL in this case.
                *(node->object_ptr) = *(to_erase->object_ptr);
                // It's okay to change the key now, because no further comparisons
                // will take place during the routine.
            }
            // to_erase is now a node with at least one NULL child.
            // this is because the successor is the _minimum_ element
            // in the right subtree.  We now need to remove to_erase.
            /*.*/ RBT_Node<T> * child = to_erase-> left;
            if (child == NULL) {child = to_erase->right;}
            // We select child as the non-null child, if such exists.
            
            // Have child take its parent's place in the tree, so that
            // to_erase may be deleted.  (Since the other child is NULL, it
            // does not need to be addressed.)
            if (to_erase == root) {root = child;}
            if (child != NULL)
            {
                child->parent = to_erase->parent;
            }
            RBT_Node<T> * childs_parent = to_erase->parent;
            
            if (to_erase->parent != NULL) /*i.e. not erasing the root*/
            {
                if (to_erase == to_erase->parent->left)
                {
                    childs_parent->left  = child;
                }
                else
                {
                    childs_parent->right = child;
                }
            }
            
            if (RBT_Node<T>::is_black(to_erase))
            {
                if (!RBT_Node<T>::is_black(child))
                {
                    /*child can't be NULL*/  child->color = BLACK;
                }
                else while (RBT_Node<T>::is_black(child))
                {
                    /*Then child is the new root, and it's black.  If
                     the old root is black, then the black height remains
                     invariant when the old root is removed.  So we're done.*/
                    if (childs_parent == NULL)
                    {
                        root = child;  break;
                    }
                    
                    /*...*/ RBT_Node<T> * sister = childs_parent-> left;
                    if (sister == child) {sister = childs_parent->right;}
                    
                    if (!RBT_Node<T>::is_black(sister))
                    {
                        //sister is not NULL
                        childs_parent->color = RED;
                        sister->color = BLACK;
                        if (child == childs_parent->left)
                        {
                            left_rotation(childs_parent);
                        }
                        else
                        {
                            right_rotation(childs_parent);
                        }
                        //childs_parent is still the parent of child
                    }
                    
                    /*.................*/ sister = childs_parent-> left;
                    if (sister == child) {sister = childs_parent->right;}
                    
                    if (RBT_Node<T>::is_black(childs_parent) &&
                        RBT_Node<T>::is_black(sister))
                    {
                        if (sister != NULL)
                        {
                            if (RBT_Node<T>::is_black(sister->left) &&
                                RBT_Node<T>::is_black(sister->right))
                            {
                                sister->color = RED;
                                child = childs_parent;
                                childs_parent = child->parent;
                                continue;
                            }
                            // From now on the loop will no longer iterate.
                        }
                    }
                    
                    if ((!RBT_Node<T>::is_black(childs_parent)) &&
                        RBT_Node<T>::is_black(sister))
                    {
                        if (sister != NULL)
                        {
                            if (RBT_Node<T>::is_black(sister->left) &&
                                RBT_Node<T>::is_black(sister->right))
                            {
                                sister->color = RED;
                                childs_parent->color = BLACK;
                                break;
                            }
                        }
                    }
                    
                    // Here it is unnecessary to reinitialize sister.
                        
                    // Sister is now necessarily black.
                    // It must have a red child of the
                    // same handedness as child to childs_parent.
                    // In particular, sister can't be NULL.
                        
                    if ((child == childs_parent->left) &&
                        RBT_Node<T>::is_black(sister->right))
                    {
                        sister->color = RED;
                        if (sister->left != NULL)
                        {
                            sister->left->color = BLACK;
                        }
                        right_rotation(sister);
                    }
                    else if ((child == childs_parent->right) &&
                            RBT_Node<T>::is_black(sister->left))
                    {
                        sister->color = RED;
                        if (sister->right != NULL)
                        {
                            sister->right->color = BLACK;
                        }
                        left_rotation(sister);
                    }
                    
                    // Having potentially rotated, we need to
                    // recalculate sister.
                    sister  = childs_parent->left;
                    if (sister == child)
                    {sister = childs_parent->right;}
                    
                    if (sister != NULL)
                    {
                        sister->color = childs_parent->color;
                    }
                    childs_parent->color = BLACK;
                                
                    if (child == childs_parent->left)
                    {
                        if (sister->right != NULL)
                        {
                            sister->right->color = BLACK;
                        }
                        left_rotation(childs_parent);
                    }
                    else
                    {
                        if (sister->left != NULL)
                        {
                            sister->left->color = BLACK;
                        }
                        right_rotation(childs_parent);
                    }
                    
                    break;
                    //End of the loop.
                }
            }
            //Erase the node to_erase.
            
            to_erase->left  = NULL; //break these links first, to avoid
            to_erase->right = NULL; //trouble when calling destructor.
            delete to_erase;
        }
        
        /** \brief Returns the black height of a node, or -1 if black-height
         * invariance is being tested, and the invariance is broken.
         *
         * @param current The node whose black height is to be computed.
         * @param invariant A flag that is set to <code>NULL</code> by
         * default.  If the flag is not <code>NULL</code>, then testing
         * for invariance
         * of the black height also takes place.
         *
         * The black height of a branch is the number of black nodes in it.
         * Since this supposed to be independent of the choice of branch, we
         * can speak of the black height of a node.
         *
         * This method is inefficient when called on every node, so it is
         * private.  It should only be used for verifying correctness of the
         * red-black tree.
         *
         * @return The black height of a node.  If black-height invariance is
         * being tested and invariance is broken, then -1 is returned.
         */
        int black_height(RBT_Node<T> * current, bool * invariant = NULL) const
        {
            if (current == NULL) {return 1;}
            int left_black_height = black_height(current->left, invariant);
            if (invariant != NULL)
            {
                if ( left_black_height < 0) {*invariant = false;  return -1;}
                int right_black_height = black_height(current->right, invariant);
                if (right_black_height < 0) {*invariant = false;  return -1;}
                if ( left_black_height != right_black_height)
                {
                    *invariant = false;  return -1;
                }
                *invariant = true;
            }
            if (RBT_Node<T>::is_black(current)) {return left_black_height + 1;}
            return left_black_height;
        }
        
        /**  \brief Private method that tests correctness of a node's black height.
         *
         * @param current The node whose height needs to be verified.
         *
         * @return <code>true</code> if the black height along the left branch
         * and right branch are equal, <code>false</code> otherwise.
         */
        bool black_height_equal(RBT_Node<T> * current) const
        {
            if (current == NULL) {return true;}
            bool invariant = true;
            int left_black_height = black_height(current->left, &invariant);
            int right_black_height = black_height(current->right, &invariant);
            if (left_black_height  < 0) {return false;}
            if (right_black_height < 0) {return false;}
            return (left_black_height == right_black_height);
        }
        
    public:
        
        /**  \brief Prints out the red-black tree as a series of nodes in
         * sorted order.
         *
         * @param str An initial message that can be displayed if desired.
         */
        void print_(const std::string & str = "") const
        {
            if (str.length() > 0) {std::cout << str << "\n";}
            for (RBT_Node<T> * it = in_order_begin(); it != NULL;
                 it = in_order_next(it))
            {
                it->print();
            }
        }
        
        /**  \brief Returns the smallest node in the tree rooted at the current node.
         *
         * @param current The current node.
         *
         * This routine takes O(log(n)) time.
         *
         * @return The smallest node in the tree rooted at the current node, or
         * <code>NULL</code> if the current node is <code>NULL</code>.
         */
        static RBT_Node<T> * find_smallest(RBT_Node<T> * current)
        {
            if (current == NULL) {return NULL;}
            RBT_Node<T> * answer = current;
            while (answer->left != NULL)
            {
                answer = answer->left;
            }
            return answer;
        }
        
        /**
         * The root is NULL and keys are unique.
         */
        RedBlackTree() {root = NULL;  unique_keys = true;}
        /**
         * Calls the RBT_Node<U>::delete_with_children() routine.
         */
        ~RedBlackTree()
        {
            if (root) {root->delete_with_children();  delete root;}
        }
        
        /** \brief Inserts a node with a given key into the red-black tree.
         *
         * This routine takes O(log(n)) time.
         *
         * @param item The key of the node to be inserted.
         *
         * If keys are unique and there is already a node with the given key,
         * the node will not be inserted.  Otherwise, a new node with the given
         * key will be created and inserted into the tree while maintaining the
         * red-black property.
         */
        void insert(const T & item)
        {
            if (root == NULL)
            {
                root = new RBT_Node<T>(item);
                root->color = BLACK;
                return;
            }
            RBT_Node<T> * current = root;
            while (true)
            {
                if ((item < *(current->object_ptr)) || (item == *(current->object_ptr)))
                {
                    if (item == *(current->object_ptr))
                    {
                        if (unique_keys) {return;}
                    }
                    if (current->left != NULL)
                    {
                        current = current->left;
                        continue;
                    }
                    current->left = new RBT_Node<T>(item);
                    current->left->parent = current;
                    current = current->left;
                    break;
                }
                if (item > *(current->object_ptr))
                {
                    if (current->right != NULL)
                    {
                        current = current->right;
                        continue;
                    }
                    current->right = new RBT_Node<T>(item);
                    current->right->parent = current;
                    current = current->right;
                    break;
                }
                break;
            }
            current->left = NULL;
            current->right = NULL;
            current->color = RED;
            
            // Maintain red-black tree property.
            
            RBT_Node<T> * par = current->parent;  //Cannot be NULL
            while (!RBT_Node<T>::is_black(par))
            {
                //par_par can't be NULL, or par, being the root, would not be red.
                RBT_Node<T> * par_par = par->parent;
                RBT_Node<T> * unc = par_par->left;
                if (unc == par) {unc = par_par->right;}
                
                if (!RBT_Node<T>::is_black(unc))
                {
                    //Then unc cannot be NULL.
                    par_par->color = RED;
                    par->color = BLACK; // 2 black children.
                    unc->color = BLACK;
                    current = par_par; //Go up two.
                    par = current->parent;
                    continue;
                }
                
                // So unc must be black here.
                
                if (par == par_par->left)
                {
                    if (current == par->right)
                    {
                        left_rotation(par);
                        par = current;
                        current = current->left;
                        par_par = par->parent;
                    }
                    par->color = BLACK;
                    par_par->color = RED;
                    
                    right_rotation(par_par);
                }
                else //if (par == par_par->right)
                {
                    if (current == par->left)
                    {
                        right_rotation(par);
                        par = current;
                        current = current->right;
                        par_par = par->parent;
                    }
                    par->color = BLACK;
                    par_par->color = RED;
                    
                    left_rotation(par_par);
                }
                
                if (current != NULL) {par = current->parent;}
                /*otherwise parent has already been set*/
            }
            
            root->color = BLACK;
        }
        
        /**\brief Removes an item if it exists in the tree.
         *
         * @param item The item to be removed.
         *
         * This routine takes O(log(n)) time.
         */
        void remove(const T& item)
        {
            RBT_Node<T> * match = find_(item);
            if (match != NULL) // O(lg(n))
            {
                remove_single_(match);
            }
        }
        
        /**\brief Removes all occurrences of an item.
         *
         * @param item The item to be removed.
         *
         * This routine takes O(log(n) * N) time, where N is
         * the number of occurrences of the item.
         */
        void remove_all(const T& item)
        {
            RBT_Node<T> * match = find_(item);
            while (match != NULL) // O(lg(n)) per iteration
            {
                remove_single_(match);
                match = find_(item);
            }
        }
        
        /** \brief Validates that the current structure satisfies the red-black
         * tree properties.
         *
         * Validates that the current structure satisfies the red-black
         * tree properties.  The test is relatively
         * slow, and should not be included in the API of a finished product.
         *
         * The following properties are tested:
         * - The blackness of the root.
         * - The fact that any red node
         * has precisely two black children (possibly <code>NULL</code>).
         * - The fact that the black height is invariant for any node.
         * - The requirement that
         * key(**left**) <= key(**current**) <= key(**right**).
         *
         * @return <code>true</code> if no red-black tree laws are violated,
         * <code>false</code> otherwise.
         */
        bool test_red_black_height_property() const
        {
            bool answer = true;
            RBT_Node<T> * it = in_order_begin();
            bool black_height_ok = true;
            bool left_comparison_ok = true;
            bool right_comparison_ok = true;
            int count = -1;
            for (; it != in_order_end(); it = in_order_next(it))
            {
                count++;
                if (it == root)
                {
                    if (!RBT_Node<T>::is_black(it))
                    {
                        answer = false;  break;
                    }
                }
                if (!RBT_Node<T>::is_black(it))
                {
                    if (!RBT_Node<T>::is_black(it->left )) {answer = false;  break;}
                    if (!RBT_Node<T>::is_black(it->right)) {answer = false;  break;}
                }
                black_height_ok = black_height_equal(it);
                
                if (!black_height_ok) {answer = false;  break;}
                {
                    int key_current = *(it->object_ptr);
                    if (it->left != NULL)
                    {
                        int comparison_key = *(it->left->object_ptr);
                        if (comparison_key > key_current)
                        {
                            left_comparison_ok = false;  answer = false;  break;
                        }
                    }
                    if (it->right != NULL)
                    {
                        int comparison_key = *(it->right->object_ptr);
                        if (comparison_key < key_current)
                        {
                            right_comparison_ok = false;  answer = false;  break;
                        }
                    }
                }
            }
            if (!answer)
            {
                bool temp_bool = false; //Line for breakpoint.
                temp_bool = true; //Eliminate warning.
            }
            return answer;
        }
        
        /** \brief Returns the *maximum* height of the binary tree rooted at the
         * current node.
         *
         * @param node The current node whose subtree-height is to be determined.
         *
         * @return The *maximum* height of the binary tree
         * rooted at the current node.
         */
        int get_height(RBT_Node<T> * node) const
        {
            if ((node == NULL) || (!node)) {return 0;}
            int left_height  = get_height(node->left);
            int right_height = get_height(node->right);
            if (right_height > left_height) {return right_height + 1;}
            /*...otherwise...............*/ {return  left_height + 1;}
        }
        
        /** \brief Returns the *maximum* height of the red-black tree.
         *
         * This routine takes O(log(n)) time.
         *
         * @return The *maximum* height of the red-black tree.
         */
        int get_height() const {return get_height(root);}
        
        /** \brief Prints the red-black tree as a sorted array.
         *
         * @param str An optional string parameter to be displayed at the beginning.
         */
        void print(const std::string & str = "") const
        {
            if (str.length() > 0) {std::cout << str << "\n(";}
            for (RBT_Node<T> * it = in_order_begin();
                 it != in_order_end(); it = in_order_next(it))
            {
                if (it != in_order_begin()) {std::cout << ", ";}
                if (it->object_ptr) {std::cout << (*it->object_ptr);}
            }
            std::cout << ")\n";
        }
        
        /** \brief Tests the soundness of the red-black tree construction.
         *
         * A random array of size *arraySize* of integers ranging
         * from 0 to 9999 is initialized.
         * Its contents are then slightly modified to preserve the order.
         * After the modification, the array contains
         * unique doubles.  The array size is set to 1000.
         *
         * A red-black tree
         * is constructed from elements in the array by a series of
         * successive insertions.  After each
         * insertion,
         * programSamples::RedBlackTree<T>::test_red_black_height_property()
         * is run to ensure that the
         * red-black property is maintained.  After the entire tree is built up,
         * it is printed in the form of a sorted array, and its height is displayed.
         *
         * Lower height indicates faster basic operations, and one can see that
         * for a size as large as 1000 the height is less than 20.
         *
         * Having tested insertion by building up the tree,
         * elements are successively removed from
         * the tree at random.  After each removal, the tree is tested to
         * make sure its red-black properties remain intact.
         * This repeats until the tree is empty.
         *
         * The routine prints whether the red-black property ultimately is
         * shown to hold or not, both for insertion and deletion, respectively.
         */
        static void test_red_black_tree()
        {
            // Random tree
            //if (false) /*comment out to have block, uncomment to skip*/
            {
                srand((unsigned) time(0));
                const int arraySize = 1000;
                double myArray[arraySize];
                for (int i = 0; i < arraySize; i++)
                {
                    float increment = 0.5 * (float) i / (float) arraySize;
                    int current = rand() % 10000;
                    myArray[i] = current + increment;
                }
                
                // Test insertion.
                RedBlackTree<double> tree;
                bool tree_ok = true;
                for (int i = 0; i < arraySize; i++)
                {
                    tree.insert(myArray[i]);
                    if (!tree.test_red_black_height_property())
                    {
                        tree_ok = false;  break;
                    }
                }
                tree.print("\n// Red Black Tree as Set:");
                if (tree_ok)
                {
                    std::cout << "\nRed-black property holds for insertion.\n";
                }
                else
                {
                    std::cout << "\nRed-black property fails for insertion.\n";
                    return;
                }
                std::cout << "\nHeight = " << tree.get_height() << "\n";
                
                // Test deletion.
                tree_ok = true;
                int count = 0;
                for (int i = arraySize - 1; i >= 0; i--)
                {
                    count++;
                    
                    // Uncomment if you want to print current tree.
                    // tree.print_("\n\nCurrent tree that we're deleting...");
                    int random_int = abs(rand()) % (i + 1);
                    RBT_Node<double> * node = tree.in_order_begin();
                    for (int j = 0; j < random_int; j++)
                    {
                        node = tree.in_order_next(node);
                    }
                    // Uncomment if you want to print current random node to delete.
                    // int key_ = *(node->object_ptr);
                    // std::cout << "\n" << key_ << "\n";
                    tree.remove_single_(node);
                    if (!tree.test_red_black_height_property())
                    {
                        tree_ok = false;  break;
                    }
                }
                tree.print("\n// Red Black Tree as Set:");
                if (tree_ok)
                {
                    std::cout << "\nRed-black property holds for deletion.\n";
                }
                else
                {
                    std::cout << "\nRed-black property fails for deletion ";
                    std::cout << "on the " << count << "'th step, starting at 1.\n";
                    return;
                }
            }
        }
    };
}

#endif
