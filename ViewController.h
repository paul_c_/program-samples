//
//  ViewController.h
//  ProgramSamplesIOS
//
//  Created by Paul Cernea on 7/12/15.
//  Copyright (c) 2015 Paul Cernea. All rights reserved.
//

#import <UIKit/UIKit.h>

/** \brief A controller for the view on the display device.
 *
 */
@interface ViewController : UIViewController
{
    /** A console for displaying output.*/
    IBOutlet UITextView * console;
    
    /** Various commands can be performed from this toolbar.*/
    IBOutlet UIToolbar * toolbar_main;
    
    /** Pressing this button performs Merge Sort.*/
    IBOutlet UIBarButtonItem * barMergeSort;
}

/** \brief By clicking the connected button, the user runs the Merge Sort test.
 *
 * @param sender Included as a parameter, but not used in the method.
 */
- (IBAction) performMergeSort : (id) sender;

@end

