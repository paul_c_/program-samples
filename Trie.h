//
//  Trie.h
//  AlgorithmsStructures
//
//  Created by Paul Cernea on 7/29/15.
//  Copyright (c) 2015 Paul Cernea. All rights reserved.
//

#ifndef AlgorithmsStructures_Trie_h
#define AlgorithmsStructures_Trie_h
#include <cstdlib>   // memory, rand
#include <iostream>  // output

/*! \file Trie.h
 \brief Implements the Trie data structure.
 */

namespace programSamples
{
    /** \brief A trie is an efficient data structure for holding words.
     *
     * @class
     * @param T The character type out of which the words will be composed.
     * Typically, this type will be <code>char</code>.
     */
    template <class T> class Trie
    {
        /** \brief A node in the trie.
         *
         * @class
         */
        struct TrieNode
        {
            /** \brief The value of the node.
             *
             */
            T symbol;
            /** \brief A pointer to the first child of the current node.
             *
             * The value must be <code>NULL</code> if the node has no children.
             */
            TrieNode * child;
            /** \brief A pointer to the first younger sibling of the current node.
             *
             * The value must be <code>NULL</code> if the node has no younger
             * siblings.
             */
            TrieNode * next;
            /** \brief This value is <code>true</code> if the node represents the
             * end of a word, and <code>false</code> otherwise.
             */
            bool is_final;
            /** Nodes are not ends of words by default.  The default symbol is
             * set by the default constructor of its type.  For characters this
             * is the null character.
             */
            TrieNode()
            {
                child = NULL;
                next = NULL;
                is_final = false;
            }
            /** \brief Recursively deletes the children of the current node.
             */
            void deleteChildren()
            {
                if (child != NULL)
                {
                    child->deleteChildren();
                    delete child;
                }
                if (next != NULL)
                {
                    next->deleteChildren();
                    delete next;
                }
            }
        };
        
        /** \brief By convention, the root has no symbol.  It
         * represents the empty string.
         */
        TrieNode * root;
        /** \brief The number of elements in the trie.
         */
        int trieSize;
        
    public:
        
        /** \brief Initializes the root.
         */
        Trie() : root(new TrieNode()), trieSize(1) {}
        /** \brief Deletes the children of the root recursively.
         */
        ~Trie()
        {
            if (root != NULL)
            {
                root->deleteChildren();
                delete root;
            }
        }
        
        /** \brief Inserts a word into the trie.
         *
         * @param arr The array to be inserted.
         * @param length The length of the array.
         *
         * This procedure takes O(length) time.
         */
        void insert(const T * arr, const int length)
        {
            TrieNode * current = root;
            for (int i = 0; i < length; i++)
            {
                bool found_this_one = false;
                const T element = arr[i];
                bool change_child = true;
                bool started = false;
                
                for (; current->child != NULL; current = current->next)
                {
                    if (!started) {current = current->child;}
                    started = true;
                    if (element == current->symbol)
                    {
                        found_this_one = true;
                        change_child = true;
                        break;
                    }
                    change_child = false;
                    if (current->next == NULL) {break;}
                }
                if (found_this_one) {continue;}
                //Otherwise target must be NULL, so we may insert.
                for (; i < length; i++)
                {
                    if (change_child)
                    {
                        current->child = new TrieNode();
                        current = current->child;
                    }
                    else
                    {
                        current->next  = new TrieNode();
                        current = current->next;
                    }
                    change_child = true;
                    trieSize++;
                    current->symbol = arr[i];
                    if (i == length - 1) {break;}
                }
                break;
            }
            current->is_final = true;
        }
        
        /** \brief Finds whether a word is in the trie.
         *
         * @param arr The array to be inserted.
         * @param length The length of the array.
         *
         * @return <code>true</code> if the word is in the trie, <code>false</code>
         * otherwise.
         *
         * This procedure takes O(length) time.
         */
        bool find(const T * arr, const int length) const
        {
            TrieNode * current = root;
            for (int i = 0; i < length; i++)
            {
                bool found_this_one = false;
                const T element = arr[i];
                bool started = false;
                
                for (;(current->child != NULL) || (started && (current != NULL)); current = current->next)
                {
                    if (!started) {current = current->child;}
                    started = true;
                    if (element == current->symbol)
                    {
                        found_this_one = true;
                        break;
                    }
                    if (current->next == NULL) {break;}
                }
                if (found_this_one) {continue;}
                return false;
            }
            if (current->is_final) {return true;}
            return false;
        }
        
        /** \brief Inserts a word into the trie.
         *
         * @param arr The null-terminated array to be inserted.
         *
         * This procedure takes O(len) time, where len is the length of the array.
         */
        void insert(const T * arr)
        {
            int len_ = 0;
            while (arr[len_]) {len_++;}
            insert(arr, len_);
        }
        
        /** \brief Finds whether a word is in the trie.
         *
         * @param arr The array to be inserted.
         *
         * @return <code>true</code> if the word is in the trie, <code>false</code>
         * otherwise.
         *
         * This procedure takes O(len) time, where len is the length of the word.
         */
        bool find(const T * arr) const
        {
            int len_ = 0;
            while (arr[len_]) {len_++;}
            return find(arr, len_);
        }
        
        /** \brief Prints every word in the trie.
         *
         * This procedure takes O(n^2) where n is the size of the trie.
         * The algorithm is essentially depth-first search.
         */
        void printAll() const
        {
            int stack_size = 0;
            int num_visited = 0;
            TrieNode * stack  [trieSize];
            TrieNode * visited[trieSize];
            
            TrieNode * current = root;
            stack[stack_size] = current;  stack_size++;
            
            std::cout << "\n";
            while (stack_size > 0)
            {
                if (current == NULL)
                {
                    stack_size--;
                    current = stack[stack_size - 1];
                    continue;
                }
                // Check whether visited.
                // This could be done more efficiently by using a set
                // data structure rather than an array (for visited).
                // However, this procedure is just for testing.
                bool current_is_visited = false;
                for (int i = 0; i < num_visited; i++)
                {
                    if (visited[i] == current)
                    {
                        current_is_visited = true;
                        break;
                    }
                }
                if (current_is_visited)
                {
                    if (current->next == NULL)
                    {
                        stack_size--;
                        current = stack[stack_size - 1];
                        continue;
                    }
                    current = current->next;
                    stack[stack_size - 1] = current;
                }
                // Neither visited nor NULL.  Moving forward.
                visited[num_visited] = current;  num_visited++;
                if (current->is_final)
                {
                    std::cout << "{ ";
                    for (int i = 1; i < stack_size; i++)
                    {
                        TrieNode * outputter = stack[i];
                        std::cout << outputter->symbol;
                    }
                    std::cout << " }\n";
                }
                if (current->child == NULL)
                {
                    current = current->next;
                    stack[stack_size - 1] = current;
                    continue;
                }
                current = current->child;
                stack[stack_size] = current;  stack_size++;
            }
        }
    };
    
    /** \brief Tests for the trie data structure.
     *
     */
    void test_trie()
    {
        srand((unsigned) time(0));
        Trie<char> tr;
        tr.insert("icthyosaurus");
        tr.insert("hello");
        tr.insert("hello"); // No memory leak here.
        tr.insert("world");
        tr.insert("");
        tr.insert("hey");
        tr.printAll();
        if (tr.find("")) {std::cout << "\"\" is in the trie.\n";}
        else {std::cout << "\"\" is not in the trie.\n";}
        if (tr.find("hey")) {std::cout << "\"hey\" is in the trie.\n";}
        else {std::cout << "\"hey\" is not in the trie.\n";}
        if (tr.find("wow")) {std::cout << "\"wow\" is in the trie.\n";}
        else {std::cout << "\"wow\" is not in the trie.\n";}
    }
}

#endif
