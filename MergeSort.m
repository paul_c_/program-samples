//
//  MergeSort.m
//  ProgramSamplesIOS
//
//  Created by Paul Cernea on 7/12/15.
//  Copyright (c) 2015 Paul Cernea. All rights reserved.
//

#import "MergeSort.h"
#include <stdlib.h>

@implementation MergeSort

-(void) merge : (NUM *) array first_index : (int) first_index half_index : (int) half_index last_index : (int) last_index
{
    int length = last_index - first_index + 1;
    
    int i = first_index;  int j = half_index;
    
    for (int k = 0; k < length; k++)
    {
        // Boundary cases.
        
        if (i == half_index)
        {
            aux_array[k] = array[j];  j++;
            continue;
        }
        if (j == last_index + 1)
        {
            aux_array[k] = array[i];  i++;
            continue;
        }
        
        // Regular cases.
        
        NUM a = array[i];  NUM b = array[j];
        if (a < b)
        {
            aux_array[k] = a;  i++;  continue;
        }
        //else
        {
            aux_array[k] = b;  j++;  //continue;
        }
    }
    
    //Place contents of auxiliary array back in original.
    
    int l = first_index;
    for (int k = 0; k < length; k++)
    {
        array[l] = aux_array[k];  l++;
    }
}

-(void) mergeSort : (NUM *) array first_index : (int) first_index last_index : (int) last_index
{
    int length = last_index - first_index + 1;
    if (length <= 1) {return;}
    int half_length = length / 2;
    int half_index = first_index + half_length;
    [self mergeSort : array first_index : first_index last_index : half_index - 1];
    [self mergeSort : array first_index : half_index  last_index : last_index];
    [self merge : array first_index : first_index half_index :  half_index last_index : last_index];
}

-(id) init
{
    if (self = [super init])
    {
        aux_array = NULL;
        aux_array_size = 0;
        return self;
    }
    else {return nil;}
}

-(void) freeMemory
{
    if (aux_array != NULL)
    {
        free(aux_array);
        aux_array_size = 0;
    }
}

-(void) mergeSort : (NUM *) array withLength : (int) arrayLength
{
    if (arrayLength > aux_array_size)
    {
        aux_array = (NUM *) calloc(arrayLength, sizeof(NUM));
        aux_array_size = arrayLength;
    }
    
    [self mergeSort : array first_index : 0 last_index : arrayLength - 1];
}

+(NSString *) print : (NUM *) array withLength : (int) arrayLength
{
    NSMutableString * output = [NSMutableString string];
    [output appendString : @"\n("];
    for (int i = 0; i < arrayLength; i++)
    {
        if (i > 0) {[output appendString : @", "];}
        [output appendFormat : @"%f", array[i]];
    }
    [output appendString : @")\n"];
    return output;
}

+(NSString *) test_mergesort
{
    NSMutableString * output = [NSMutableString string];
    // Random doubles.
    {
        double myArray[70];
        for (int i = 0; i < 70; i++)
        {
            int current = arc4random_uniform(9999);
            myArray[i] = current + 0.5;
        }
        
        [output appendString : @"\nUnsorted.\n"];
        [output appendString : [MergeSort print : myArray withLength : 70]];
        
        MergeSort * m = [[MergeSort alloc] init];
        [m mergeSort : myArray withLength : 70];
        [m freeMemory];
        
        [output appendString : @"\nMerge sorted.\n" ];
        [output appendString : [MergeSort print : myArray withLength : 70]];
    }
    return output;
}
@end
