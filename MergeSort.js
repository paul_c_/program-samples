/**
* @file MergeSort.js
* Created on 7/11/15.
* Copyright (c) 2015 Paul Cernea.  All rights reserved.
* @author Paul Cernea
*/

/**
* An environment for containing the Merge Sort implementation and avoiding naming clashes.
* The namespace will override any existing namespace called programSamples.
* @namespace
*/
var programSamples =
{
	/** An auxiliary array for memory management during merging.*/
	aux_array : [],

	/** The size of the auxiliary array.*/
	aux_array_size : 0,

	/**Merges two sorted arrays.
	*
	* @param arr An array containing the two sorted arrays
	* to be merged.  They are stored contiguously.
	* @param first_index The first index of the entire array.
	* Also, the first index of
	* the first sorted array to be merged.
	* @param half_index The first index of the second sorted array to be merged.
	* Also, half_index - 1 is the last index of the first sorted array to be
	* merged.
	* @param last_index The last index of the second sorted array to be merged.
	*/
	merge : function(arr, first_index, half_index, last_index)
	{
		var length_ = last_index - first_index + 1;
	    
	    var i = first_index;  var j = half_index;

	    for (var k = 0; k < length_; k++)
	    {
	        // Boundary cases.

	        if (i == half_index)
	        {
	            programSamples.aux_array[k] = arr[j];  j++;
	            continue;
	        }
	        if (j == last_index + 1)
	        {
	            programSamples.aux_array[k] = arr[i];  i++;
	            continue;
	        }

	        // Regular cases.

	        var a = arr[i];  var b = arr[j];
	        if (a < b)
	        {
	            programSamples.aux_array[k] = a;  i++;  continue;
	        }
	        //else
	        {
	            programSamples.aux_array[k] = b;  j++;  //continue;
	        }
	    }

	    //Place contents of auxiliary array back in original.
	        
	    var l = first_index;
	    for (var k = 0; k < length_; k++)
	    {
	        arr[l] = programSamples.aux_array[k];  l++;
	    }
	},
	    
	/** Recursively sorts an array.
	*
	* The recursive sort proceeds as follows:  If the array has fewer than two
	* elements, there is nothing more to be done.  Otherwise, split the array
	* in half.  Recursively sort both halves.  Then perform a linear-time
	* merge to combine the arrays.
	* The entire procedure takes O(n * log(n)) time.
	* @param arr The array to be merge-sorted.
	* @param first_index The first index of the array.
	* @param last_index The last index of the array.
	*/
	mergeSort_ : function(arr, first_index, last_index)
	{
	    var length = last_index - first_index + 1;
	    if (length <= 1) {return;}
	    var half_length = Math.floor(length / 2);
	    var half_index = first_index + half_length;
	    programSamples.mergeSort_(arr, first_index, half_index - 1);
	    programSamples.mergeSort_(arr, half_index, last_index);
	    programSamples.merge(arr, first_index, half_index, last_index);
	},
	    
	/** Initializes the auxiliary array,
	* and sets the auxiliary array size to 0.
	*/
	init : function()
	{
	    programSamples.aux_array = [];  programSamples.aux_array_size = 0;
	},

	/** Performs Merge Sort on an array of length arrayLength.
	*
	* Performs Merge Sort on an array of length arrayLength in O(n * log(n))
	* time, where n = arrayLength.  The method will terminate
	* if it cannot allocate the necessary O(n)
	* extra memory to perform the sort.  It uses the private method
	* MergeSort.mergeSort(Array, int, int)
	* as a subroutine.
	* @param arr The array to be sorted.
	* @param arrayLength The length of the array to be sorted.
	*/
	mergeSort : function(arr, arrayLength)
	{
	    if (arrayLength > programSamples.aux_array_size)
	    {
	        var size_ = programSamples.aux_array.length;
	        
	        for (var i = size_; i < arrayLength; i++)
	        {
	            programSamples.aux_array.push(0.0);
	        }
	        
	        programSamples.aux_array_size = arrayLength;
	    }

	    programSamples.mergeSort_(arr, 0, arrayLength - 1);
	},

	/** Outputs an array of length arrayLength into a string.
	* @param arr The array to be printed.
	* @param arrayLength The length of the array to be printed.
	* @return The array in the form of a string.
	*/
	print_ : function(arr, arrayLength)
	{
		var outputter = "\n(";
	    for (var i = 0; i < arrayLength; i++)
	    {
	        if (i > 0) {outputter += ", ";}
	        outputter += arr[i];
	    }
	    outputter += ")\n";
	    
	    return outputter;
	},

	/** Tests Merge Sort on an array of 70 pseudo-random numbers.
	 *
	 * The pseudo-random numbers are initialized to be between 0 and 9999, inclusive.  Then
	 * they are stored as floats with 0.5 added to them.  The unsorted and sorted
	 * arrays are printed.
	 */
	test_mergesort : function()
	{
	        var myArray = [];
	        var arraySize = 70;
	        for (var i = 0; i < arraySize; i++)
	        {
	        	var current = Math.floor(Math.random() * 10000);
	            var curr_ = current + 0.5;
	            myArray.push(curr_);
	        }
	        
	        var toDisplay = "\nUnsorted.\n";
	        toDisplay += programSamples.print_(myArray, arraySize);
	        
	        programSamples.init();
	        
	        programSamples.mergeSort(myArray, arraySize);
	        
	        toDisplay += "\nMerge sorted.\n";
	        toDisplay += programSamples.print_(myArray, arraySize);
	        
	        window.alert(toDisplay);

	}
} //end namespace declaration

// Run the program.
programSamples.test_mergesort();
