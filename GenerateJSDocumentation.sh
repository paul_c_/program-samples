
JSDOC_DIR=#Replace this comment with the directory where jsrun.jar is located.

SAMPLES_DIR=#Replace this comment with your JavaScript program samples directory.

# 1. Copy the MergeSort javascript file into the JSDoc directory.
cp MergeSort.js $JSDOC_DIR

# 2. Generate the corresponding html file, and open it in the browser.
cd $JSDOC_DIR
java -jar jsrun.jar app/run.js -a -t=templates/jsdoc MergeSort.js
open ./out/jsdoc/index.html

# 3. Compress the html files into a .zip file.
cd out/jsdoc
zip -r jsdoc.zip .

# 4. Copy the zipped html file into the Program Samples directory, and return there.
cp jsdoc.zip $SAMPLES_DIR
rm jsdoc.zip
cd $SAMPLES_DIR