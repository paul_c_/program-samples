//
//  MergeSort.h
//  AlgorithmsStructures
//
//  Created by Paul Cernea on 7/7/15.
//  Copyright (c) 2015 Paul Cernea. All rights reserved.
//

#ifndef AlgorithmsStructures_MergeSort_h
#define AlgorithmsStructures_MergeSort_h
#include <cstdlib>   // memory, rand
#include <iostream>  // output

/*! \file MergeSort.h
 \brief Implements the Merge Sort sorting algorithm.
 */

namespace programSamples
{
    /** \brief Instantiate once in order to run Merge Sort.
     *
     * @param NUM A template argument representing the type of object that will be sorted.  If NUM is a custom type, the less-than operator < must
     * be overloaded.
     */
    template <class NUM> class MergeSortInstance
    {
        NUM * aux_array;  /**< An auxiliary array for memory management during merging.*/
        int aux_array_size;  /**< The size of the auxiliary array.*/
        
        /** \brief Merges two sorted arrays.
         * @param array An array of NUM containing the two sorted arrays
         * to be merged.  They are stored contiguously.
         * @param first_index The first index of the entire array.
         * Also, the first index of
         * the first sorted array to be merged.
         * @param half_index The first index of the second sorted array to be merged.
         * Also, half_index - 1 is the last index of the first sorted array to be
         * merged.
         * @param last_index The last index of the second sorted array to be merged.
         */
        void merge(NUM * array, int first_index, int half_index, int last_index)
        {
            int length = last_index - first_index + 1;
            
            int i = first_index;  int j = half_index;
            
            for (int k = 0; k < length; k++)
            {
                // Boundary cases.
                
                if (i == half_index)
                {
                    aux_array[k] = array[j];  j++;
                    continue;
                }
                if (j == last_index + 1)
                {
                    aux_array[k] = array[i];  i++;
                    continue;
                }
                
                // Regular cases.
                
                NUM a = array[i];  NUM b = array[j];
                if (a < b)
                {
                    aux_array[k] = a;  i++;  continue;
                }
                //else
                {
                    aux_array[k] = b;  j++;  //continue;
                }
            }
            
            //Place contents of auxiliary array back in original.
            
            int l = first_index;
            for (int k = 0; k < length; k++)
            {
                array[l] = aux_array[k];  l++;
            }
        }
        
        /** \brief Recursively sorts an array.
         *
         * The recursive sort proceeds as follows:  If the array has fewer than two
         * elements, there is nothing more to be done.  Otherwise, split the array
         * in half.  Recursively sort both halves.  Then perform a linear-time
         * merge to combine the arrays.
         * The entire procedure takes O(n * log(n)) time.
         * @param array The array to be merge-sorted.
         * @param first_index The first index of the array.
         * @param last_index The last index of the array.
         */
        void mergeSort(NUM * array, int first_index, int last_index)
        {
            int length = last_index - first_index + 1;
            if (length <= 1) {return;}
            int half_length = length / 2;
            int half_index = first_index + half_length;
            mergeSort(array, first_index, half_index - 1);
            mergeSort(array, half_index, last_index);
            merge(array, first_index, half_index, last_index);
        }
        
    public:
        
        /** \brief Initializes the auxiliary array to NULL
         * and the auxiliary array size to 0.
         */
        MergeSortInstance() : aux_array(NULL), aux_array_size(0) {}
        
        /** \brief Frees the auxiliary array if the array
         * is not null.
         */
        ~MergeSortInstance()
        {
            if (aux_array != NULL) {free(aux_array);}
        }
        
        /** \brief Performs Merge Sort on an array of length arrayLength.
         *
         * Performs Merge Sort on an array of length arrayLength in O(n * log(n))
         * time, where n = arrayLength.  The method will terminate
         * if it cannot allocate the necessary O(n)
         * extra memory to perform the sort.  It uses the private method
         * programSamples::MergeSortInstance<NUM>::mergeSort(NUM *, int, int)
         * as a subroutine.
         * @param array The array to be sorted.
         * @param arrayLength The length of the array to be sorted.
         * @throw
         *  An out-of-memory exception is thrown if there is no more memory for
         *  the auxiliary array.
         */
        void mergeSort(NUM * array, int arrayLength)
        {
            if (arrayLength > aux_array_size)
            {
                try
                {
                    if (aux_array == NULL)
                    {
                        aux_array = (NUM *) calloc(arrayLength, sizeof(NUM));
                    }
                    else
                    {
                        aux_array = (NUM *) realloc(aux_array, arrayLength * sizeof(NUM));
                    }
                    
                    if (aux_array == NULL)
                    {
                        throw "\nOut of memory.";
                    }
                }
                catch (const char * msg)
                {
                    std::cerr << msg << std::endl;  return;
                }
                
                aux_array_size = arrayLength;
            }
            
            mergeSort(array, 0, arrayLength - 1);
        }
        
        /** \brief Prints an array of length arrayLength.
         * @param array The array to be printed.
         * @param arrayLength The length of the array to be printed.
         */
        static void print(NUM * array, int arrayLength)
        {
            std::cout << "\n(";
            for (int i = 0; i < arrayLength; i++)
            {
                if (i > 0) {std::cout << ", ";}
                std::cout << array[i];
            }
            std::cout << ")\n";
        }
    };

    /** \brief Tests Merge Sort on an array of 70 pseudo-random numbers.
     *
     * The pseudo-random numbers have had the modulo operator applied to them to
     * make them less than 10,000.  This makes them visually easier to read.  Then
     * they are stored as doubles with 0.5 added to them.  The unsorted and sorted
     * arrays are printed.
     */
    void test_mergesort()
    {
        // Random doubles.
        {
            srand((unsigned) time(0));
            double myArray[70];
            for (int i = 0; i < 70; i++)
            {
                int current = rand() % 10000;
                myArray[i] = current + 0.5;
            }
            
            std::cout << "\nUnsorted.\n";
            MergeSortInstance<double>::print(myArray, 70);
            
            MergeSortInstance<double> mergeSort;
            mergeSort.mergeSort(myArray, 70);
            
            std::cout << "\nMerge sorted.\n";
            MergeSortInstance<double>::print(myArray, 70);
        }

    }
}

#endif
